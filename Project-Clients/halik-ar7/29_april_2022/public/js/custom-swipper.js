const swiper = new Swiper('.swiper-slider', {
	// spacebetween itu, Mengatur jarak renggang bagian gambar penawaran menarik
	spaceBetween: 30,
	breakpoints: {
		280: {
			spaceBetween: 20,
			slidesPerView: 1.2,
		},
		576: {
			slidesPerView: 2,
		},
		640: {
			slidesPerView: 2.5,
		},
		768: {
			spaceBetween: 40,
			slidesPerView: 2.5,
		},
		940: {
			spaceBetween: 40,
			slidesPerView: 3,
		},
	},
	autoplay: {
		// Waktu autoplay nya itu berapa detik, disini menulis 4000 artinya 4detik. Itungan dalam satuan ms
		delay: 4000,
		disableOnInteraction: false,
	},
	loop: true,
	// If we need pagination
	pagination: {
		el: '.swiper-pagination',
		clickable: true,
	},
});

const swiperVideos = new Swiper('.swiper-videos', {
	direction: 'vertical',
});
