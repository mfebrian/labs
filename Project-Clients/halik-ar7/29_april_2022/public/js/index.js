// * Mengambil element tag body
const body = document.body;

// * Menampilkan menu mobile
const menuMobile = document.getElementById('menu-mobile');
const hamburgerMenu = document.getElementById('hamburger-menu');

//* Ini ketika bagian icon menu di klik, ketika tampilan mobile akan memunculkan link menu
hamburgerMenu.addEventListener('click', function () {
	menuMobile.classList.add('w-full');
	body.style.overflow = 'hidden';
});

// * Menutup link menu
const closeMenu = document.getElementById('close-menu');
closeMenu.addEventListener('click', function () {
	menuMobile.classList.remove('w-full');
	body.removeAttribute('style');
});

// * Mengambil element yang mempunyai class list-header
const listsHeader = document.querySelectorAll('.list-header');
// * Mengambil element yang mempunyai class indicator
const indicator = document.querySelector('.indicator');

// * Lakukan foreach pada listHeader untuk mengetahui tiap element / tag nya
listsHeader.forEach((listHeader) => {
	// * Menjalankan event click, ketika kita mengclick listHeader atau pembungkus icon menu ketika versi mobile
	listHeader.addEventListener('click', function (e) {
		e.preventDefault();

		// * Menghapus semua listHeader yang mempunyai class active
		listsHeader.forEach((listHeaderNonActive) => {
			listHeaderNonActive.classList.remove('active');
		});

		// * Ketika semua listHeader diapus diatas, lalu listHeader yang kita klik akan menambahka class active
		listHeader.classList.add('active');

		// * Mengambil list icon
		const listIcon = listHeader.querySelector('.list-icon-header');

		// * Mengetahui jarak element dari kiri, apabila bagian lingkaran kurang sejajar secara horizontal dengan icon, bisa dikurangi atau ditambah (disesuaikan)
		const positionIcon = listIcon.getBoundingClientRect().left - 16;
		indicator.style.transform = `translateX(${positionIcon}px)`;
	});
});

const listIconActive = document.querySelector('.list-header.active');

// * List header yang paling kanan posisi nya atau paling akhir, maka jarak posisi nya akan dikurangi 8px. Jika kurang sejajar secara horizontal dengan icon, bisa dikurangi atau ditambah (disesuaikan)
if (listsHeader[listsHeader.length - 1] === listIconActive) {
	indicator.style.transform = `translateX(${listIconActive.getBoundingClientRect().left - 8}px)`;
} else {
	// * Jika bukan, bagian paling kanan atau akhir dikurangi 1px, jika kurang sejajar secara horizontal dengan icon, bisa dikurangi atau ditambah (disesuaikan)
	indicator.style.transform = `translateX(${listIconActive.getBoundingClientRect().left - 1}px)`;
}

// Bagian popup video
const wrapperVideos = document.querySelector('#wrapper-videos');

const seeMores = document.querySelectorAll('.see-more');
seeMores.forEach((seeMore) => {
	const description = seeMore.previousElementSibling;
	if (description.textContent.length >= 89) {
		seeMore.classList.replace('hidden', 'block');
	} else {
		seeMore.classList.replace('block', 'hidden');
	}

	seeMore.addEventListener('click', function () {
		const wrapperDescription = seeMore.previousElementSibling;
		wrapperDescription.classList.toggle('line-clamp-2');

		if (wrapperDescription.classList.contains('line-clamp-2')) {
			seeMore.textContent = 'See More';
		} else {
			seeMore.textContent = 'See Less';
		}
	});
});

// Stop video yang tidak dilihat
const videos = document.querySelectorAll('video');
let options = {
	threshold: 1.0,
};

function callback(entries) {
	entries.forEach((entri) => {
		const playVideo = [...entri.target.parentElement.children].slice(-1)[0];
		if (entri.isIntersecting) {
			entri.target.play();
			playVideo.classList.replace('flex', 'hidden');
		} else {
			playVideo.classList.replace('hidden', 'flex');
			entri.target.pause();
			entri.target.currentTime = 0;
		}
	});
}

let observer = new IntersectionObserver(callback, options);
for (const video of videos) {
	observer.observe(video);
	video.addEventListener('click', function () {
		const playVideo = [...video.parentElement.children].slice(-1)[0];
		if (playVideo.classList.contains('hidden')) {
			video.pause();
			playVideo.classList.replace('hidden', 'flex');
		} else {
			playVideo.classList.replace('flex', 'hidden');
			video.play();
		}
	});
}

// Klik icon dan buka popup tiktok clone
const iconLayananKhusus = document.querySelectorAll('[data-id]');
const popupVideos = document.getElementById('popup-videos');
iconLayananKhusus.forEach((iconLK) => {
	iconLK.addEventListener('click', function (e) {
		e.preventDefault();

		body.classList.add('overflow-hidden');
		popupVideos.classList.replace('hidden', 'grid');
		const id = this.dataset.id;
		const selectedVideo = wrapperVideos.querySelector(`#${id}`);

		if (selectedVideo != null) {
			wrapperVideos.scrollTo(0, selectedVideo.offsetTop);
			selectedVideo.children[0].play();
			wrapperVideos.requestFullscreen();
		} else {
			wrapperVideos.scrollTo(0, 0);
			wrapperVideos.children[0].children[0].play();
		}
	});
});

function closePopupVideos() {
	popupVideos.classList.replace('grid', 'hidden');
	body.classList.remove('overflow-hidden');
}

window.addEventListener('click', function (e) {
	if (e.target.id == 'popup-videos') {
		closePopupVideos();
	}
});

wrapperVideos.addEventListener('fullscreenchange', function () {
	if (!document.fullscreenElement) {
		closePopupVideos();
	}
});
