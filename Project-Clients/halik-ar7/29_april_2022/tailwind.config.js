module.exports = {
	content: ['./*.html', './public/js/*.js'],
	theme: {
		extend: {
			borderColor: {
				'header-bottom': '#DFDFDF',
			},
			fontFamily: {
				nunito: ['Nunito', 'sans-serif'],
			},
			colors: {
				primary: '#E0004D',
				'menu-mobile': '#818181',
				'menu-mobile-list': '#dee2e6',
			},
			textColor: {
				primary: '#333333',
				secondary: '#E0004D',
				third: '#53565A',
				quote: '#718096',
			},
			backgroundColor: {
				hero: '#F6F8FC',
				'card-hero': '#E1E8F5',
				footer: '#284450',
				copyright: '#1C3641',
			},
			gridTemplateColumns: {
				'two-max-content': 'max-content max-content',
			},
			screens: {
				xs: '400px',
			},
		},
	},
	plugins: [require('@tailwindcss/line-clamp')],
};
