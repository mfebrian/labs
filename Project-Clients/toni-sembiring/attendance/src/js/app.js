const body = document.body;

// Bagian header untuk popup profile, ketika di klik
const profile = document.querySelector('.wrapper-photo-profile-header');
const popupProfile = document.querySelector('.popup--profile');
profile.addEventListener('click', function () {
	popupProfile.style.display = 'block';
});

function closePopup(target) {
	target.style.display = 'none';
}

// Menghilangkan link untuk dropdown dibagian sidebar
const linksDropdown = document.querySelectorAll('.dropdown > .page-list-menu');
linksDropdown.forEach((linkDropdown) => {
	linkDropdown.addEventListener('click', function (e) {
		e.preventDefault();

		const iconArrowDropdown = linkDropdown.querySelector('.icon-dropdown');
		iconArrowDropdown.classList.toggle('rotate');

		// Animasi untuk dropdown
		const subDropdown = this.parentElement.querySelector('.sub-dropdown');
		subDropdown.classList.toggle('open');
	});
});

// Ketika klik link di sidebar, maka akan nemanbahkan class active untuk menambah background color
const linksSidebar = document.querySelectorAll('.page-list-menu:not(.dropdown > .page-list-menu)');
linksSidebar.forEach((linkSidebar) => {
	linkSidebar.addEventListener('click', function () {
		linksSidebar.forEach((linkSidebarNonActive) => {
			linkSidebarNonActive.classList.remove('active');
		});

		this.classList.add('active');
	});
});

// Klik hamburger menu nanti akan muncul sidebar nya
const menu = document.getElementById('menu');

const sidebar = document.querySelector('.list-menu');
menu.addEventListener('click', function () {
	sidebar.classList.toggle('open');
	closeSidebar();
});

// Membuat fungsi untuk menutup sidebar
function closeSidebar() {
	const movedContent = document.querySelectorAll('.moved-list-menu');

	movedContent.forEach((move) => {
		if (!move.hasAttribute('style')) {
			if (window.innerWidth <= 380) {
				move.style.transform = `translateX(${sidebar.clientWidth}px)`;
			} else {
				move.style.marginLeft = `${sidebar.clientWidth}px`;
			}
			body.classList.add('overflow-x-hidden');
		} else {
			move.removeAttribute('style');
			sidebar.classList.remove('open');
			body.removeAttribute('class');
		}
	});
}

// History kembali ke halaman sebelumnya
const backPage = document.querySelector('.back-page');
if (backPage !== null) {
	backPage.addEventListener('click', function () {
		window.history.back();
	});
}

// Event klik ketika bagian add image di klik
const addImageText = document.querySelector('.wrapper-text-choose-image');
const wrapperInputsFile = document.querySelector('.wrapper-inputs-file');
const wrapperTextChooseImage = document.querySelector('.wrapper-text-choose-image');

if (addImageText !== null) {
	addImageText.addEventListener('click', function () {
		const addImageMaximum = 6;

		if (wrapperInputsFile.children.length < addImageMaximum) {
			createElementInputFileAddImage();
			wrapperTextChooseImage.classList.remove('disabled-add-image');
		} else {
			wrapperTextChooseImage.classList.add('disabled-add-image');
		}

		const iconsDelete = document.querySelectorAll('.wrapper-input-file svg');
		iconsDelete.forEach((iconDelete) => {
			deleteInputFileAddImage(iconDelete);
		});
	});
}

function createElementInputFileAddImage() {
	// Membuat element div untuk pembungkus input file
	const divElement = document.createElement('div');
	divElement.setAttribute('class', 'wrapper-input-file');

	// Membuat element input dengan tipe file
	const inputElement = document.createElement('input');
	inputElement.setAttribute('type', 'file');
	// Memberikan attribute accept karena ingin memperbolehkan gambar dengan ekstensi: png, jpg, dan jpeg
	inputElement.setAttribute('accept', '.png, .jpg, .jpeg');

	// Membuat element icon/svg untuk delete input file
	const divIconDelete = document.createElement('div');
	divIconDelete.innerHTML = `<svg width="24" height="24" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
	<path
		d="M17 6.72132H22V8.74457H20V21.8957C20 22.164 19.8946 22.4213 19.7071 22.6111C19.5196 22.8008 19.2652 22.9074 19 22.9074H5C4.73478 22.9074 4.48043 22.8008 4.29289 22.6111C4.10536 22.4213 4 22.164 4 21.8957V8.74457H2V6.72132H7V3.68643C7 3.41813 7.10536 3.16082 7.29289 2.9711C7.48043 2.78139 7.73478 2.6748 8 2.6748H16C16.2652 2.6748 16.5196 2.78139 16.7071 2.9711C16.8946 3.16082 17 3.41813 17 3.68643V6.72132ZM18 8.74457H6V20.8841H18V8.74457ZM9 11.7795H11V17.8492H9V11.7795ZM13 11.7795H15V17.8492H13V11.7795ZM9 4.69806V6.72132H15V4.69806H9Z"
		fill="red"
	/>
</svg>`;

	divElement.append(inputElement, divIconDelete);
	wrapperInputsFile.append(divElement);
}

// Fungsi untuk menghapus input file ketika di klik icon delete nya
function deleteInputFileAddImage(iconDelete) {
	iconDelete.addEventListener('click', function () {
		const inputFile = iconDelete.parentElement.parentElement;
		wrapperTextChooseImage.classList.remove('disabled-add-image');
		inputFile.remove();
	});
}

// Ketika mengubah gambar utama saat thumbnail di klik
const imageBigSize = document.querySelector('.image-big-size img');
const thumbnails = document.querySelectorAll('.wrapper-device-image-thumbnail img');

for (const thumbnail of thumbnails) {
	thumbnail.addEventListener('click', function () {
		setTimeout(() => {
			imageBigSize.parentElement.classList.remove('check');
		}, 0);

		setTimeout(() => {
			imageBigSize.src = this.src;
			imageBigSize.parentElement.classList.add('check');
		}, 250);
	});
}

// Bagian thumbnail agar dinamis sesuai dengan jumlah thumbnail
const wrapperThumbnail = document.querySelector('.wrapper-device-image-thumbnail');

if (wrapperThumbnail !== null) {
	wrapperThumbnail.style.cssText = `grid-template-columns:repeat(${thumbnails.length}, 1fr);`;
}

// Popup muncul ketika meng-klik perangkat baru / icon action edit
const popupAction = document.querySelector('.popup--action');
const btnFormAction = document.querySelector('.wrapper-btn-action-form button[type="submit"]');

function formAction(action) {
	if (popupAction !== null) {
		popupAction.classList.add('show');

		const [actionAdd, actionEdit] = popupAction.children;
		const btnAction = action.dataset.btnAction;

		if (btnAction === 'add') {
			actionAdd.classList.add('show');
			actionEdit.classList.remove('show');
		} else {
			actionEdit.classList.add('show');
			actionAdd.classList.remove('show');
		}
	}
}

// Event ketika klik tombol perangkat baru dan akan menampilkan popup action
const btnAddDevice = document.querySelector('.btn--add-device');
if (btnAddDevice !== null) {
	btnAddDevice.addEventListener('click', function () {
		formAction(btnAddDevice);
	});
}

// Event ketika klik tombol edit dan akan menampilkan popup action
const buttonsEditFormAction = document.querySelectorAll('.icon-action--edit');
for (const btnEditFormAction of buttonsEditFormAction) {
	btnEditFormAction.addEventListener('click', function () {
		formAction(btnEditFormAction);
	});
}

// Hapus semua event popup action ketika tampil
function removePopupAction() {
	popupAction.classList.remove('show');
	removeSubPopupAction();
	body.removeAttribute('style');
}

// Hapus subpopup action form edit / add
function removeSubPopupAction() {
	const subPopupActions = popupAction.querySelectorAll('.sub-popup-action');

	// Loop sub popup action
	for (const subPopupAction of subPopupActions) {
		subPopupAction.classList.remove('show');
	}
}

// Event ketika tombol close di klik, dan akan menghapus popup action
const btnCancelFormAction = document.querySelector('.btn--action-form.btn--delete ');
if (btnCancelFormAction !== null) {
	btnCancelFormAction.addEventListener('click', function () {
		removePopupAction();
	});
}

// Event ketika text approve di klik akan menampilkan popup form approve
const requestStatusApprove = document.querySelectorAll('.request-status--approve');
for (const statusApprove of requestStatusApprove) {
	statusApprove.addEventListener('click', function () {
		const action = `<div class="sub-popup-action sub-popup-action-w-34 sub-popup-action--static">
				<p class="title-popup-action title-popup-action--center">Approve Data Permintaan</p>

				<form method="post" class="form-action form-action--request-data">
					<!-- Kolom data jenis Pekerjaan -->
					<div>
						<h3 class="subtitle-page">Jenis Pekerjaan</h3>

						<div class="body-input-form-action">
							<div class="wrapper-column-create">
								<div class="column-create">
									<label for="jenis-pekerjaan" class="description-data description-data--column-create">Jenis Pekerjaan</label>
									<select name="jenis-pekerjaan" id="jenis-pekerjaan" class="input">
										<option value="Frontend Developer">Frontend Developer</option>
										<option value="Backend Developer">Backend Developer</option>
										<option value="Mobile Developer">Mobile Developer</option>
										<option value="Fullstack Developer">Fullstack Developer</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<!-- Kolom data jenis peralatan/barang/jasa -->
					<div>
						<h3 class="subtitle-page">Jenis Peralatan/Barang/Jasa</h3>

						<div class="body-input-form-action">
							<div class="wrapper-column-create">
								<div class="column-create">
									<label for="nama-peralatan" class="description-data description-data--column-create">Nama</label>
									<input type="text" name="nama-peralatan" id="nama-peralatan" class="input" />
								</div>

								<div class="column-create">
									<label for="jumlah-peralatan" class="description-data description-data--column-create">Jumlah</label>
									<input type="text" name="jumlah-peralatan" id="jumlah-peralatan" class="input" />
								</div>

								<div class="column-create">
									<label for="merek" class="description-data description-data--column-create">Merek</label>
									<input type="text" name="merek" id="merek" class="input" />
								</div>

								<div class="column-create">
									<label for="department" class="description-data description-data--column-create">Dept/Divisi</label>
									<input type="text" name="department" id="department" class="input" />
								</div>
							</div>
						</div>
					</div>

					<!-- Kolom data pemohon -->
					<div>
						<h3 class="subtitle-page">Pemohon</h3>

						<div class="body-input-form-action">
							<div class="wrapper-column-create">
								<div class="column-create">
									<label for="nama-pemohon" class="description-data description-data--column-create">Nama</label>
									<input type="text" name="nama-pemohon" id="nama-pemohon" class="input" />
								</div>

								<div class="column-create">
									<label for="tanggal-pemohon" class="description-data description-data--column-create">Tanggal</label>
									<input type="date" name="tanggal-pemohon" id="tanggal-pemohon" class="input" />
								</div>

								<div class="column-create">
									<label for="jabatan-pemohon" class="description-data description-data--column-create">Jabatan</label>
									<input type="text" name="jabatan-pemohon" id="jabatan-pemohon" class="input" />
								</div>
							</div>
						</div>
					</div>

					<!-- Kolom data penerima -->
					<div>
						<h3 class="subtitle-page">Penerima</h3>

						<div class="body-input-form-action">
							<div class="wrapper-column-create">
								<div class="column-create">
									<label for="nama-penerima" class="description-data description-data--column-create">Nama</label>
									<input type="text" name="nama-penerima" id="nama-penerima" class="input" />
								</div>

								<div class="column-create">
									<label for="tanggal-penerima" class="description-data description-data--column-create">Tanggal</label>
									<input type="date" name="tanggal-penerima" id="tanggal-penerima" class="input" />
								</div>

								<div class="column-create">
									<label for="jabatan-penerima" class="description-data description-data--column-create">Jabatan</label>
									<input type="text" name="jabatan-penerima" id="jabatan-penerima" class="input" />
								</div>
							</div>
						</div>
					</div>

					<div class="wrapper-btn-action-form">
						<button class="btn btn--create btn--action-form" type="submit" name="approve">Approve</button>
					</div>
				</form>
			</div>`;

		requestStatus(action);
	});
}

const requestStatusFinish = document.querySelectorAll('.request-status--finish');
for (const statusFinish of requestStatusFinish) {
	statusFinish.addEventListener('click', function () {
		const action = `<div class="sub-popup-action sub-popup-action-w-34 sub-popup-action--static">
				<p class="title-popup-action title-popup-action--center">Penyelesaian Data Permintaan</p>

				<form method="post" class="form-action form-action--request-data">
					<!-- Kolom data hasil pekerjaan -->
					<div>
						<h3 class="subtitle-page">Hasil Pekerjaan</h3>

						<div class="body-input-form-action">
							<div class="wrapper-column-create">
								<div class="column-create">
									<label for="tanggal-hasil-pekerjaan" class="description-data description-data--column-create">Tanggal</label>
									<input type="date" name="tanggal-hasil-pekerjaan" id="tanggal-hasil-pekerjaan" class="input" />
								</div>

								<div class="column-create">
									<label for="jenis-pekerjaan" class="description-data description-data--column-create">Jenis Pekerjaan</label>
									<select name="jenis-pekerjaan" id="jenis-pekerjaan" class="input">
										<option value="Frontend Developer">Frontend Developer</option>
										<option value="Backend Developer">Backend Developer</option>
										<option value="Mobile Developer">Mobile Developer</option>
										<option value="Fullstack Developer">Fullstack Developer</option>
									</select>
								</div>

								<div class="column-create">
									<label for="waktu-hasil-pekerjaan" class="description-data description-data--column-create">Waktu</label>
									<input type="time" name="waktu-hasil-pekerjaan" id="waktu-hasil-pekerjaan" class="input" />
								</div>
							</div>

							<div class="column-create column-create--inherit">
								<label for="deskripsi-hasil-pekerjaan" class="description-data description-data--column-create">Uraian Pekerjaan <sup class="required">*</sup></label>
								<textarea name="deskripsi-hasil-pekerjaan" id="deskripsi-hasil-pekerjaan" rows="7" class="input" required></textarea>
							</div>

							<div class="column-create column-create--inherit">
								<label for="rekomendasi-hasil-pekerjaan" class="description-data description-data--column-create">Rekomendasi <sup class="required">*</sup></label>
								<textarea name="rekomendasi-hasil-pekerjaan" id="rekomendasi-hasil-pekerjaan" rows="7" class="input" required></textarea>
							</div>
						</div>
					</div>

					<div class="wrapper-btn-action-form">
						<button class="btn btn--finish btn--action-form" type="submit" name="finish">Finish</button>
					</div>
				</form>
			</div>`;

		requestStatus(action);
	});
}

function requestStatus(action) {
	popupAction.classList.add('show');
	body.style.overflow = 'hidden';
	popupAction.innerHTML = action;
}

// Event ketika tombol Escape di tekan
window.addEventListener('keyup', function ({ key }) {
	if (key === 'Escape') {
		if (popupProfile.hasAttribute('style')) {
			closePopup(popupProfile);
		}

		if (sidebar.classList.contains('open')) {
			closeSidebar();
		}

		if (popupAction != null && !popupAction.classList.contains('popup-user')) {
			removePopupAction();
		}
	}
});

// Event ketika window di klik
window.addEventListener('click', function ({ target }) {
	// Ketika ada elemen html yang punya class popup--profile, maka akan menjalankan fungsi diatas, yaitu. Menghilangkan popup profile header
	if (target.classList.contains('popup--profile')) {
		closePopup(popupProfile);
	}

	if (target.classList.contains('popup--action') && !popupAction.classList.contains('popup-user')) {
		removePopupAction();
	}
});

window.addEventListener('load', function () {
	const popupUser = document.querySelector('.popup-user');
	if (popupUser !== null) {
		popupUser.classList.add('show');
	}
});

// Event ketika star di pilih/klik
const starRatings = document.querySelectorAll('.star');
for (const [currentStar, star] of starRatings.entries()) {
	star.addEventListener('click', function () {
		for (let starNonActive = 0; starNonActive < starRatings.length; starNonActive++) {
			starRatings[starNonActive].innerHTML = `<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="M16 24.3465L6.59605 29.6105L8.69605 19.0398L0.782715 11.7225L11.4854 10.4532L16 0.666504L20.5147 10.4532L31.2174 11.7225L23.3041 19.0398L25.404 29.6105L16 24.3465ZM16 21.2905L21.6627 24.4598L20.3974 18.0958L25.1614 13.6892L18.7174 12.9252L16 7.03317L13.2827 12.9265L6.83872 13.6892L11.6027 18.0958L10.3374 24.4598L16 21.2905Z"
				fill="#FFE40E"
			/>
		</svg>`;

			starRatings[starNonActive].classList.remove('active-star');
		}

		for (let starActive = 0; starActive <= currentStar; starActive++) {
			starRatings[starActive].innerHTML = `<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M16 24.3465L6.59605 29.6105L8.69605 19.0398L0.782715 11.7225L11.4854 10.4532L16 0.666504L20.5147 10.4532L31.2174 11.7225L23.3041 19.0398L25.404 29.6105L16 24.3465Z" fill="#FFE40E"/>
				</svg>`;

			starRatings[starActive].classList.add('active-star');
		}
	});
}

// Event ketika klik tombol submit pada popup di user
const submitRating = document.getElementById('submit-rating');
if (submitRating !== null) {
	submitRating.addEventListener('click', function () {
		const starActive = document.querySelectorAll('.active-star');
		const requiredRating = document.querySelector('.give-rating');

		if (starActive.length === 0) {
			requiredRating.classList.add('error');
		} else {
			requiredRating.style.visibility = 'hidden';
			removePopupAction();
		}
	});
}
