const currentDate = document.querySelector('.day');
let day = new Date().getDay();
const date = new Date().getDate();
let month = new Date().getMonth();
const year = new Date().getFullYear();

switch (day) {
  case 0:
    day = 'Minggu';
    break;
  case 1:
    day = 'Senin';
    break;
  case 2:
    day = 'Selasa';
    break;
  case 3:
    day = 'Rabu';
    break;
  case 4:
    day = 'Kamis';
    break;
  case 5:
    day = "Jum'at";
    break;
  case 6:
    day = 'Sabtu';
    break;
  default:
    console.log('Tanggal salah');
    break;
}

switch (month) {
  case 0:
    month = 'Januari';
    break;
  case 1:
    month = 'Februari';
    break;
  case 2:
    month = 'Maret';
    break;
  case 3:
    month = 'April';
    break;
  case 4:
    month = 'Mei';
    break;
  case 5:
    month = 'Juni';
    break;
  case 6:
    month = 'Juli';
    break;
  case 7:
    month = 'Agustus';
    break;
  case 8:
    month = 'September';
    break;
  case 9:
    month = 'Oktober';
    break;
  case 10:
    month = 'November';
    break;
  case 11:
    month = 'Desember';
    break;
  default:
    console.log('Bulan salah');
    break;
}

currentDate.innerHTML = `${day}, <span class="current-date">${date} ${month} ${year}</span>`;

const timeNow = document.querySelector('.time-now');
setInterval(() => {
  const hours = new Date().getHours();
  const minutes = new Date().getMinutes();
  const seconds = new Date().getSeconds();
  timeNow.textContent = `${hours}:${minutes < 10 ? `0${minutes}` : `${minutes}`}:${seconds}`;
}, 1000);

const menu = document.querySelector('.menu');
const schedule = document.querySelector('.schedule');
menu.addEventListener('click', () => {
  schedule.classList.toggle('show');
});

window.addEventListener('resize', () => {
  schedule.classList.remove('show');
});

const swiperWrapper = document.querySelector('.swiper-wrapper');
const swiperSlide = Array.from(document.querySelectorAll('.swiper-slide'));
swiperSlide.forEach((slide, index) => {
  switch (index) {
    case swiperSlide.length - 1:
    case 1:
      slide.style.backgroundImage = 'url(public/asset/img/bg.jpg)';
      break;
    case 2:
      slide.style.backgroundImage = 'url(https://wallpaperaccess.com/full/39632.jpg)';
      break;
    case 3:
      slide.style.backgroundImage = 'url(https://wallpaperaccess.com/full/1314136.jpg)';
      break;
    case 4:
      slide.style.backgroundImage =
        'url(https://previews.123rf.com/images/maximusnd/maximusnd1804/maximusnd180401662/98995577-beautiful-nebula-stars-and-galaxies-.jpg)';
      break;
    case 5:
      slide.style.backgroundImage = 'url(https://img.freepik.com/free-vector/realistic-galaxy-background_23-2148991322.jpg?size=626&ext=jpg)';
      break;
    case 6:
      slide.style.backgroundImage =
        'url(https://media.istockphoto.com/photos/purple-space-stars-picture-id157639696?k=20&m=157639696&s=612x612&w=0&h=SdmnpR_xWbXDcd36SggDN0hPX18CqgUjgZ1WQsAK_18=)';
      break;
    case 7:
      slide.style.backgroundImage =
        'url(https://images.unsplash.com/photo-1528722828814-77b9b83aafb2?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8Mnx8fGVufDB8fHx8&w=1000&q=80)';
      break;
    case 8:
      slide.style.backgroundImage = 'url(https://images.pexels.com/photos/110854/pexels-photo-110854.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500)';
      break;
    case 9:
      slide.style.backgroundImage = 'url(https://cdn.pixabay.com/photo/2017/08/08/00/33/constellations-2609647__340.jpg)';
      break;
    case 0:
    case 10:
      slide.style.backgroundImage =
        'url(https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/panorama-milky-way-galaxy-with-stars-and-space-dust-royalty-free-image-1623176828.jpg?crop=1xw:0.88893xh;center,top&resize=1200:*)';
      break;
  }
});
