const navigation = document.querySelector('.navigation');
window.addEventListener('scroll', function () {
	if (this.window.scrollY >= 10) {
		navigation.classList.add('scroll');
	} else {
		navigation.classList.remove('scroll');
	}
});

const listNavigation = document.querySelectorAll('.list-navigation');
listNavigation.forEach((list) => {
	list.addEventListener('click', function (e) {
		if (this.href == '#') {
			e.preventDefault();
		}

		listNavigation.forEach((listNav) => {
			listNav.classList.remove('active');
		});

		list.classList.add('active');
	});
});
