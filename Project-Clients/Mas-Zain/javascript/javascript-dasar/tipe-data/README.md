!['Javascript Code'](https://images.unsplash.com/photo-1550063873-ab792950096b?ixlib=rb-1.2.1&raw_url=true&q=80&fm=jpg&crop=entropy&cs=tinysrgb&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870)

# Beberapa Tipe Data Javascript

Setiap bahasa pemrograman pasti didalamnya memiliki tipe data, javascript pun demikian. Setidaknya ada 6 tipe data didalam javascript, yaitu **String**, **Number**, **Boolean**, **Undefined**, **Function** dan **Object**.

## String

String merupakan tipe data yang berisikan text, dimana text ini harus berada didalam sebuah kutip satu (') atau kutip dua ("). Yang dimana pembuka kutip harus sama dengan penutupnya. Contoh jika kita menuliskan string dengan kutip satu.

**Cara penulisan yang benar**
`const name = 'John Doe';`

Ini merupakan cara penulisan yang benar, dimana kita membuka nya dengan kutip satu, lalu menutupnya juga dengan kutip satu.

Kita abaikan bagian `const name`, karena itu merupana sebuah variable. Yang dimana nilai string John Doe. Akan masuk kedalam variable name. Pembahasan tentang variable nanti akan dijelaskan dimateri lain.

**Cara penulisan yang salah**
`const name = 'John Doe";`

Ini merupakan cara penulisan yang salah, dimana kita membuka nya dengan kutip satu, lalu menutupnya dengan kutip dua. Karena string pembukanya menggunakan kutip satu, akan tetapi penutupnya menggunakan kutip dua, maka ini akan menghasilkan error **Uncaught SyntaxError: Invalid or unexpected token**.

Begitupun sebaliknya, ketika kita menggunakan kutip dua. Kita harus mengawalinya dengan kutip dua, lalu mengakhiri atau menutupnya dengan kutip dua juga.

**Cara penulisan yang benar**
`const name = "John Doe";`

**Cara penulisan yang salah**
`const name = "John Doe';`

Namun semenjak fitur terbaru ES6 atau ECMAScript6 yang rilis pada tahun 2015. Javascript memiliki gaya penulisan baru pada string yang disebut _Tempate Literals_ atau _Template Strings_, dimana string pembuka nya diawali menggunakan backtick (`) begitupun juga dengan penutupnya.

Contoh
`` const username = `Doe John`; ``

Keunggunalan kita menggunakan Template Strings adalah, kita dapat memberikan baris baru pada text nya atau disebut dengan _Multi-line strings_

Contoh

```
const person = `Hi there,
my name is John Doe
i am 24 years old`;
```

Dan keunggulan lain adalah, kita dapat memberikan sebuah _expression_ yang perintahnya adalah `${expression}`. Dibuka dengan tanda dolar dan curly brackets (kurung kurawal) dan didalamnya bisa expression nya. Expression bisa berisi nama variable, function, loop, conditional, number dan bahkan template strings lagi atau biasa disebut _nested template literals_. Untuk lebih lanjut mengenai template strings ini, kamu bisa membacanya di [MDN-Template_Literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals).

## Number

!['Number'](https://images.unsplash.com/photo-1529078155058-5d716f45d604?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=869)

Yang sudah kita ketahui, tipe data ini merupakan tipe data yang berisikan angka-angka. Di tipe data number ini ada 2 bagian, yaitu _Floating Point (Bilangan desimal)_ dan _Integers (Bilangan bulat)_. Dimana untuk bagian integers atau bilangan bulat kita mungkin sudah tahu, seperti `2,3,4,5,6,` dan seterusnya. Sedangkan bagian floating point atau bilangan desimal contohnya seperti `2.34, 3.12, 4.20` dan seterusnya, dimana ada koma. Namun didalam javascript ataupun bahasa pemrograman lainnya. Dalam **menuliskan koma kita harus menggunakan titik**.

## Boolean

Tipe data ini hanya mencakup dua bagian saja, yaitu **true** dan **false**. Walaupun hanya bagian, tipe data boolean ini merupakan bagian yang sering dipakai, salah satunya saat melakukan conditional. Namun kita harus mengetahui juga nilai mana yang mengandung true _(truthy)_ atau mengandung false _(falsy)_.

Berikut nilai yang mengandung false (falsy):

| Value       | Description |
| ----------- | ----------- |
| false       | The keyword false |
| 0           | The Number zero (so, also 0.0, etc., and 0x0). |
| -0          | The Number negative zero (so, also -0.0, etc., and -0x0). |
| 0n          | The BigInt zero (so, also 0x0n). Note that there is no BigInt negative zero — the negation of 0n is 0n. |
| "", '', ``, | Empty string value. |
| null        | null — the absence of any value. |
| undefined   | undefined — the primitive value. |
| NaN         | NaN — not a number. |

Adapun nilai yang mengandung true (truthy): Nilai-nilai yang tidak ada di nilai falsy atau kecuali nilai-nilai dari `false`, `0`, `-0`, `0n`, `""`, `''`, ` `` `, `null`, `undefined`, dan `NaN`.

## Undefined

Undefined merupakan tipe data yang mana salah satunya jika sebuah variable tidak diberikan nilai. Maka akan menghasilkan undefined.

Contohnya:
`let name;` 

Jika kita tulis seperti ini, maka variable name akan berisi atau bernilai undefined. Mengakses sebuah properti object yang tidak ada juga bisa menghasilkan undefined. Contohnya:
```
const person = {
	name: 'John Doe',
	age: 20,
	isMarried: false,
};

console.log(person.child)
```

Atau ketika kita mengakses index array yang tidak ada. Contohnya:
```
const numbers = [1, 2, 3];
console.log(numbers[3]);
```

## Function

Function adalah tipe data ketika kita membuat sebuah function, entah itu menggunakan regular function ataupun _arrow function_ (Sintaks ES6). Berikut cara penulisan function.
```
function namaFunction() {
	console.log('Hai, aku adalah function');
}

namaFunction();
```

Dimana kita menulis nama function itu bebas aja yang gak boleh jika kita menggunakan nama function yang sudaha ada di reserved word, dan dialam kurung kita bisa berikan parameter (dimateri lain). Lalu didalam block kurung kurawal nya bisa berikan perintah kode apa yang ingin di eksekusi. Di contoh kasus ini, kita ingin meng-eksekusi perintah `console.log()`. Dan setelah kita membuat function, diakhir kita panggil atau jalankan function tersebut.

## Object

Tipe data object sering kali digunakan jika data yang diterima itu banyak karena mengelola nya yang cukup mudah. Didalam object terdapat dua kata kunci, yaitu **key** dan **value**.

Berikut contoh membuat sebuah object:
```
const myWife = {
	name: 'Angelina',
	age: 32,
	'is married': true,
	gender: 'female',
};
```

Terdapat beberapa key dan value, dimana key berada diposisi kiri dan value diposisi kanan. Jika kamu ingin memberikan spasi pada bagian key, kamu harus memberikan kutip satu atau dua (masuk sebagai string).

Perlu diingat, jika value nya bukan sebuah function maka itu kita sebut **property**. Namun jika valuenya berisi function, itu kita sebut **method**. Contoh object yang menerapkan method:
```
const calculateNeeds = {
	salary: 5000000,
	countSalary: function (dailyNeeds) {
		return this.salary -= dailyNeeds;
	},
};
```

Untuk memanggil nilai yang ada didalam object kamu bisa gunakan dua cara.
1. Gunakan notasi dot (.)
2. Gunakan kurung siku ([])

Cara pertama adalah notasi dot, berikut pemanggilannya: `console.log(myWife.name);`. Disini kita sederhana aja eksekusi object nya, yaitu mencetak menggunakan `console.log`. Pertama kita panggil nama object nya, lalu dilanjutkan dengan titik dan nama key nya.

Cara kedua adalah kurung siku, berikut pemanggilannya: `console.log(myWife['age']);`. Masih menggunakan `console.log` disini kita setelah memanggil nama object nya dilanjutkan dengan kurung siku, lalu key object nya sebagai string (bisa kutip satu atau kutip dua).

Lalu apa perbedaannya?
Salah satu perbedaannya adalah, ketika kita menggunakan kurung siku, kita bisa memanggil key object yang ada spasi nya atau lebih dari satu kata. Contohnya jika kita ingin mengambil nilai dari key `'is married'`.

Jika kita menggunakan notasi dot `console.log(myWife.is married);` seperti ini, itu akan error. Begitupun jika penulisannya seperti `console.log(myWife.'is married');`. Lalu solusinya gimana? Gimana cara ambil nilai nya?

Jadi solusinya, kamu bisa menggunakan cara kedua yaitu menggunakan kurung siku, berikut perintah nya `console.log(myWife['is married']);`. Jika kamu jalankan ini akan mencetak nilai `true`.

## Muncul Pertanyaan?

Mungkin kamu udah sadar ya atau ingin coba bertanya, kok array gak ada? atau apa array gak termasuk tipe data? 

Oke untuk menjawab itu, penulis akan kasih tahu ke kamu bahwa ada perintah itu mengecek sebuah tipe data di javascript. Perintahnya adalah `typeof`, coba kamu buat sebuah array. Lalu cek tipe data array tersebut. Contohnya:
```
const fruits = ['Mango', 'Apple', 'Banana', 'Orange'];

console.log(typeof fruits); // object
```

Jika kamu mencoba nya, maka akan menghasilkan nilai object, yang artinya adalah array ini termasuk kedalam tipe data object.

## Kesimpulan

Javascript mempunyai beberapa tipe data, yaitu String, Number, Boolean, Undefined, Function dan Object. Lalu array termasuk kedalam tipe data object. Untuk mengecek sebuah tipe data bisa gunakan `typeof` di javascript.

Sumber referensi:
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
- https://codingstudio.id/tipe-data-javascript/
- https://developer.mozilla.org/en-US/docs/Glossary/Truthy
- https://developer.mozilla.org/en-US/docs/Glossary/Falsy