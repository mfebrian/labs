!['Laptop with Code'](https://images.unsplash.com/photo-1587620962725-abab7fe55159?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1031)

Pada tahun 2020 dan 2021 menurut stackoverflow, Javascript merupakan bahasa pemrograman yang paling populer. Jadi untuk kalian yang sudah belajar ataupun yang baru ingin belajar mengenai javascript, kalian sudah berada di jalan yang benar :D

Dimana javascript sekarang sudah bisa melakukan apapun. Tidak hanya di website saja, bisa dilakukan untuk pembuatan AI (Artificial Intelligence), pembuatan aplikasi desktop, pembuatan aplikasi mobile, dan masih banyak lagi. Jadi di artikel ini, kita akan membahas mengenai javascript dan sebelum belajar javascript lebih lanjut, kita akan membahas mengenai teori nya terlebih dahulu. Dan untuk materi ini kita akan membahas mengenai **Sejarah Javascript**. Dan kalian sedang berada di bagian **Javascript Dasar**.

Sebelum memulai, alangkah baiknya kalian menyiapkan cemilan terlebih dahulu, karena materi nya lumayan panjang. Jangan sampai ketiduran karena terlalu panjang :"). Mari kita mulai.

# Sejarah Javascript

## Pembuatan di Netscape

Pada tahun 1993 release web browser pertama dengan Graphical User Interface (GUI) yaitu web browser _Mosaic_ yang dibuat oleh National Center for Supercomputing Applications _(NCSA)_. **Mosaic merupakan salah satu web browser pertama yang populer karena sudah meng-integrasikan multimedia seperti text dan grafis**, salah satu grafis adalah gambar. 
!['Illustration browser'](World-wide-web_Outline.svg)
Sedangkan NCSA adalah kemitraan negara-federal untuk mengembangkan dan menyebarkan infrastruktur siber skala nasional yang memajukan penelitian, sains dan teknik yang berbasis di United States.

Mosaic ini dapat diakses oleh orang-orang non-teknis, sehingga memainkan peran penting dalam pertumbuhan pesat pada web browser Mosaic ini. Para pengembang Mosaic kemudian mendirikan perusahaanya sendiri yang diberi nama _Netscape_ atau yang sekarang kita kenal adalah _Mozilla_ Perusahaan Netscape ini juga membuat web browser dari Mosaic yang mereka berinama _Netscape Navigator_ pada tahun 1994. Web browser Netscape Navigator ini dengan cepat menjadi yang paling banyak digunakan.

Selama bertahun-tahun pembentukan web ini, halaman web hanya bisa melakukan hal yang statis saja, tidak memiliki kemampuan untuk berperilaku dinamis atau interaktif setelah halaman browser dimuat. Para pengembang Netscape ingin menerapkan halaman web yang dinamis, sehingga pada tahun 1995, Netscape memutuskan untuk menambahkan bahasa script ke web browser mereka yaitu Netscape Navigator. Mereka mengejar atau mencari dua cara untuk mencapai ini, yaitu berkolaborasi dengan perusahaan _Sun Microsystem_. Sun ini adalah perusahaan teknologi dari Amerika yang menjual komputer, komponen komputer, software, dan layanan teknologi informasi dan yang **membuat bahasa pemrograman _Java_** yang mana kita ketahui, pada saat itu Java merupakan bahasa yang paling populer.

Netscape ingin berkolaborasi dengan Sun untuk menggabungkan bahasa Java kedalam web browser Netscape Navigator. Selain berkolaborasi dengan Sun, Netscape juga mempekerjakan seseorang yang bernama _Brendan Eich_ untuk menggabungkan bahasa pemrograman _Scheme_ kedalam web browser Netscape Navigator juga.

Lalu Netscape segera memutuskan bahwa pilihan terbaik bagi Brendan Eich adalah untuk membuat bahasa baru dengan sintaks yang mirip dengan bahasa Java dan kurang seperti bahasa Schema atau bahasa script lain yang masih ada. Meskipun bahasa baru dan implementasi interpreter disebut LiveScript saat pertama kali dikirim sebagai bagian dari web browser Netscape Navigator beta pada September 1995. Kemudian saat rilis resmi pada bulan Desember namanya diubah lagi menjadi JavaScript.

Pilihan nama JavaScript telah meyebabkan kebingungan, menyiratkan bahwa itu terkait langsung dengan Java. Karena pada saat itu bahasa Java juga salah satu yang paling populer jadi Brendan Eich menganggap penamaan JavaScript adalah sebagai **taktik pemasaran** oleh Netscape.

## Adopsi oleh Microsoft

!['Web Development'](Web-development.svg)

Microsoft memulai debutnya pada browser _Internet Explorer_ pada tahun 1995. Dan mencoba melakukan perang browser dengan Netscape. Microsoft melakukan _reverse-engineered_ pada interpreter web browser Netscape Navigator untuk membuat JavaScript versi nya Microsoft sendiri, dan Microsoft menamai nya _JScript_. JScript pertama kali rilis pada tahun 1996, bertepatan dengan awal untuk CSS dan ekstensi ke HTML.

## Munculnya JScript

!['Web Developer'](Web-Developer.svg)

Pada tahun 1996, Netscape mengirimkan JavaScript kepada ECMA International, sebagai point awal untuk men-standarkan spesifikasi di semua browser agar semua vendor bisa sesuai. Lalu pada bulan Juni 1997 dirilis resmi dari spesifikasi bahasa ECMAScript pertama. Proses standar berlanjut selama beberapa tahun, dengan dirilisnya ECMAScript 2 atau ES2 pada bulan Juni 1998 dan ECMAScript 3 pada bulan Desember 1999. Lalu pengerjaan ECMAScript 4 dimulai pada tahun 2000. Microsoft awalnya berpartisipasi dalam proses standar dan menerapkan beberapa proposal dalam bahasa JScript nya, tetapi akhirnya berhenti berkolaborasi pada pekerjaan ECMA. Sehingga ECMAScript 4 tidak dilanjutkan atau tidak digunakan lagi.

## Perkembangan ECMAScript

1. ECMAScript 1 - Juni 1997
2. ECMAScript 2 - Juni 1998
3. ECMAScript 3 - Desember 1999
4. ECMAScript 4 - 30 Juni 2003 (Ditinggalkan)
5. ECMAScript 5 - Desember 2009
6. ECMAScript 5.1 - Juni 2011 
7. ECMAScript 6 - Juni 2015
8. ECMAScript 7 - Juni 2016
9. ECMASCript 8 - Juni 2017
10. ECMASCript 9 - Juni 2018
11. ECMASCript 10 - Juni 2019
12. ECMASCript 11 - Juni 2020
13. ECMASCript 12 - Juni 2021

## Kesimpulan

Awalnya javascript dibuat untuk membuat web browser Netscape Navigator agar bisa menerapkan dinamis atau interaktif, sehingga Netscape berkolaborasi dengan Sun Microsystem yang merupakan perusahaan yang membuat bahasa Java, dimana pada saat itu bahasa Java sedang populer. Selain berkolaborasi dengan perusahaan Sun, Netscape juga mempekerjakan seseorang yang bernama Brendan Eich. Lalu Netscape segera memutuskan bahwa pilihan terbaik bagi Brendan Eich adalah untuk membuat bahasa baru dengan sintaks yang mirip dengan bahasa Java dan kurang seperti bahasa Schema atau bahasa script lain yang masih ada.

Pada awalnya nama bahasa baru dan implementasi interpreter adalah LiveScript. Kemudian pada saat rilis resmi pada bulan Desember 1995 diubah kembali menjadi JavaScript. Dan pada tahun 1996 Netscape mulai men-standarkan JavaScript kepada ECMA International agar semua browser dan semua vendor bisa sesuai.

Jadi itu merupakan sejarah mengenai JavaScript, apakah kalian masih ada yang lihat materi ini sampai habis. Atau jangan-jangan sudah  ketiduran :D. Sekian dan Terima kasih.

Sumber referensi: 
- Survei Javascript 2021: https://insights.stackoverflow.com/survey/2021#most-popular-technologies-language
- Survei Javascript 2020: https://insights.stackoverflow.com/survey/2020#most-popular-technologies
- Sejarah javascript: https://en.wikipedia.org/wiki/JavaScript#History
- Web browser Mosaic: https://en.wikipedia.org/wiki/Mosaic_(web_browser)
- Apa itu NCSA: https://en.wikipedia.org/wiki/National_Center_for_Supercomputing_Applications
- Apa itu Sun Microsystem: https://en.wikipedia.org/wiki/Sun_Microsystems
- Perkembangan ECMASCript: https://en.wikipedia.org/wiki/ECMAScript#Versions