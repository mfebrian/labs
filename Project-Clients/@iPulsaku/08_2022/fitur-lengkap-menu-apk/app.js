// Bagian other menu di scroll makan input search nya akan fixed / ikut ke scroll
const mainOtherMenu = document.querySelector('.main-other-menu');
const headerSearchFixed = document.querySelector('.header-search');
window.addEventListener('scroll', function () {
	if (this.window.scrollY >= mainOtherMenu.offsetTop) {
		headerSearchFixed.classList.add('fixed');
	} else {
		headerSearchFixed.classList.remove('fixed');
	}
});

// Menutup menu ketika dropdown di klik
const dropdowns = document.querySelectorAll('.dropdown-other-menu');
dropdowns.forEach((dropdown) => {
	dropdown.addEventListener('click', function () {
		const listMenu = this.parentElement.nextElementSibling;
		listMenu.classList.toggle('hidden');
		this.classList.toggle('rotate');
	});
});
