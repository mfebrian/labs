const dropdowns = document.querySelectorAll('.link-menu--dropdown');
dropdowns.forEach((dropdown) => {
	dropdown.addEventListener('click', function (e) {
		e.preventDefault();

		const subDropdown = dropdown.parentElement.querySelector('.sub-dropdown');
		subDropdown.classList.toggle('show');
	});
});
