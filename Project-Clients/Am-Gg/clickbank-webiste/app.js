// Buat nampilin menu ketika lagi ada diukuran mobile, ketika tombol menu (garis 3) di klik
const menu = document.getElementById('menu');
menu.addEventListener('click', function () {
	const containerLinks = document.querySelector('.links');
	containerLinks.classList.toggle('active-menu');
});

// Menampilkan dropdown
const dropdowns = document.querySelectorAll('.link--dropdown');
for (const dropdown of dropdowns) {
	dropdown.addEventListener('click', function (e) {
		e.preventDefault();
		const subDropdown = document.querySelector('.sub-dropdown');
		subDropdown.classList.toggle('active-dropdown');
	});
}

// Mengaktifkan link pada navbar/menu
const links = document.querySelectorAll('.link');
links.forEach((link) => {
	link.addEventListener('click', function () {
		links.forEach((linksNonActive) => {
			linksNonActive.classList.remove('active');
		});

		link.classList.add('active');
	});
});
