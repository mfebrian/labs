// Letak Gambar Provider
const gambarAxis = 'https://www.bhinneka.com/blog/wp-content/uploads/2022/04/Cara-Cek-Kuota-Axis-1280x720.jpg';
const gambarTri = 'https://www.bhinneka.com/blog/wp-content/uploads/2022/04/Cara-Cek-Kuota-Axis-1280x720.jpg';
const gambarIndosat = 'https://b.cloudcomputing.id/images/cee3b092-36f7-4f9e-857f-7afe512e08a9/indosar-ooredoo-l-min.jpg';
const gambarSmartfren = 'https://img.harianjogja.com/posts/2019/07/17/1006070/smartfren-ok.jpg';
const gambarXL = 'https://staticxl.ext.xlaxiata.co.id/s3fs-public/media/images/big-xl-logo.png';
const gambarTelkomsel = 'https://pbs.twimg.com/profile_images/1405865624232882176/uA2xXUFP_400x400.jpg';

// * Kode untuk javascript bagian tampilan
const svgDiscount = `<?xml version="1.0" encoding="UTF-8"?>
<svg xmlns="http://www.w3.org/2000/svg" id="Filled" viewBox="0 0 24 24" width="20" height="20" class="icon-discount">
   <style type="text/css">
					.st0 {
						fill-rule: evenodd;
						clip-rule: evenodd;
					}
				</style>
   <path class="st0" d="M2.849,23.55a2.954,2.954,0,0,0,3.266-.644L12,17.053l5.885,5.853a2.956,2.956,0,0,0,2.1.881,3.05,3.05,0,0,0,1.17-.237A2.953,2.953,0,0,0,23,20.779V5a5.006,5.006,0,0,0-5-5H6A5.006,5.006,0,0,0,1,5V20.779A2.953,2.953,0,0,0,2.849,23.55Z"/></svg>`;

const providerName = document.querySelector('.nama-provider');
const imgProvider = document.querySelector('.img-provider');

function changeProviderContent(provider = 'Operator', linkGambar = 'headphone-headset-icon.png') {
	providerName.textContent = provider;

	imgProvider.setAttribute('src', linkGambar);
}

const inputNomorTelepon = document.querySelector('.input-nomor-telepon');
const clearInput = document.querySelector('.clear-input');
const wrapperIsiPulsa = document.querySelector('.wrapper-isi-pulsa');
const wrapperPaketData = document.querySelector('.wrapper-paket-data');

const clearInputTrigger = () => {
	clearInput.classList.remove('hidden');

	if (inputNomorTelepon.value === '') {
		clearInput.classList.add('hidden');
	}
};

function conditionUpdateUIProvider(nomorTelepon) {
	const provider = Providers;
	const getFourDigitNumberPhone = nomorTelepon.slice(0, 4);
	// Nomor telepon Telkomsel
	if (
		getFourDigitNumberPhone == 0811 ||
		getFourDigitNumberPhone == 0812 ||
		getFourDigitNumberPhone == 0813 ||
		getFourDigitNumberPhone == 0821 ||
		getFourDigitNumberPhone == 0822 ||
		getFourDigitNumberPhone == 0852 ||
		getFourDigitNumberPhone == 0853 ||
		getFourDigitNumberPhone == 0823 ||
		getFourDigitNumberPhone == 0851 ||
		getFourDigitNumberPhone == 0835
	) {
		changeProviderContent('Telkomsel', gambarTelkomsel);

		const telkomsel = provider.Telkomsel;
		UiIisiPulsa(telkomsel['Isi-Pulsa'], telkomsel['Paket-Data']);
		eventClickCardPulsa();

		// Nomor Axis
	} else if (getFourDigitNumberPhone == 0838 || getFourDigitNumberPhone == 0831 || getFourDigitNumberPhone == 0832 || getFourDigitNumberPhone == 0833) {
		changeProviderContent('Axis', gambarAxis);

		const axis = provider.Axis;
		UiIisiPulsa(axis['Isi-Pulsa'], axis['Paket-Data']);
		eventClickCardPulsa();

		// Nomor Tri
	} else if (getFourDigitNumberPhone == 0895 || getFourDigitNumberPhone == 0896 || getFourDigitNumberPhone == 0897 || getFourDigitNumberPhone == 0898 || getFourDigitNumberPhone == 0899) {
		changeProviderContent('Tri', gambarTri);

		const tri = provider.Tri;
		UiIisiPulsa(tri['Isi-Pulsa'], tri['Paket-Data']);
		eventClickCardPulsa();

		// Indosat
	} else if (
		getFourDigitNumberPhone == 0814 ||
		getFourDigitNumberPhone == 0815 ||
		getFourDigitNumberPhone == 0816 ||
		getFourDigitNumberPhone == 0855 ||
		getFourDigitNumberPhone == 0856 ||
		getFourDigitNumberPhone == 0857 ||
		getFourDigitNumberPhone == 0858
	) {
		changeProviderContent('Indosat', gambarIndosat);

		const indosat = provider.Indosat;
		UiIisiPulsa(indosat['Isi-Pulsa'], indosat['Paket-Data']);
		eventClickCardPulsa();

		// Nomor XL
	} else if (getFourDigitNumberPhone == 0818 || getFourDigitNumberPhone == 0817 || getFourDigitNumberPhone == 0819 || getFourDigitNumberPhone == 0859 || getFourDigitNumberPhone == 0877 || getFourDigitNumberPhone == 0878) {
		changeProviderContent('XL', gambarXL);

		const xl = provider.XL;
		UiIisiPulsa(xl['Isi-Pulsa'], xl['Paket-Data']);
		eventClickCardPulsa();

		// Nomor Smartfreen
	} else if (
		getFourDigitNumberPhone == 0881 ||
		getFourDigitNumberPhone == 0882 ||
		getFourDigitNumberPhone == 0883 ||
		getFourDigitNumberPhone == 0884 ||
		getFourDigitNumberPhone == 0885 ||
		getFourDigitNumberPhone == 0886 ||
		getFourDigitNumberPhone == 0887 ||
		getFourDigitNumberPhone == 0888 ||
		getFourDigitNumberPhone == 0889
	) {
		changeProviderContent('Smartfren', gambarSmartfren);

		const smartfren = provider.Smartfren;
		UiIisiPulsa(smartfren['Isi-Pulsa'], smartfren['Paket-Data']);
		eventClickCardPulsa();
	}
}

function UiIisiPulsa(isiPulsa, paketData) {
	let contentIsiPulsa = '';

	isiPulsa.forEach((pulsa) => {
		contentIsiPulsa += `<a href="#" class="card-pembelian card-pembelian--isi-pulsa" ${pulsa.link !== undefined ? `data-link="${pulsa.link}"` : ''}>
								${pulsa['harga-discount'] !== undefined ? svgDiscount : ''}
								<p class="pulsa">${pulsa['total-pulsa']}</p>
								<div>
									<span class="text-harga">Harga ${pulsa['harga-discount'] !== undefined ? `<span class="harga-coret">${pulsa['harga-pulsa']}</span>` : ''}</span>
									${pulsa['harga-discount'] !== undefined ? `<p class="harga harga--discount">Rp${pulsa['harga-discount']}</p>` : `<p class="harga">Rp${pulsa['harga-pulsa']}</p>`}
								</div>
							</a>`;
	});

	wrapperIsiPulsa.innerHTML = contentIsiPulsa;

	let contentPaketData = '';

	paketData.forEach((paketData) => {
		if (paketData !== undefined) {
			contentPaketData += `<a href="#" class="card-pembelian card-pembelian--paket-data" ${paketData.link !== undefined ? `data-link="${paketData.link}"` : ''}>
									<div class="header-paket-data">
										<p class="nama-paket-data">${paketData['nama-paket']}</p>
										<span class="lihat-detail">Lihat Detail</span>
									</div>
									<p class="deskripsi-paket">${paketData['deskripsi-paket']}</p>
									${paketData['harga-paket'] !== undefined ? `<p class="harga-paket harga">Rp${paketData['harga-paket']}</p>` : ''}
								</a>`;
		}
	});

	wrapperPaketData.innerHTML = contentPaketData;
}

clearInput.addEventListener('click', function () {
	inputNomorTelepon.value = '';
	clearInput.classList.add('hidden');
	body.removeAttribute('class');

	wrapperIsiPulsa.innerHTML = '';
	wrapperPaketData.innerHTML = '';

	changeProviderContent();
});

const tabs = document.querySelectorAll('.tab');
tabs.forEach((tab) => {
	tab.addEventListener('click', function (e) {
		e.preventDefault();
		tabs.forEach((tabNonActive) => {
			tabNonActive.classList.remove('tab-active');
		});

		tab.classList.add('tab-active');

		const wrapperPembelian = document.querySelectorAll('.konten-pembelian');
		wrapperPembelian.forEach((pembelian) => {
			const datasetTabPembelian = tab.dataset.tab;
			const datasetPembelian = pembelian.dataset.target;
			if (datasetTabPembelian === datasetPembelian) {
				pembelian.classList.remove('pembelian-hidden');
			} else {
				pembelian.classList.add('pembelian-hidden');
			}
		});
	});
});

const popupKonfirmasiPembelian = document.querySelector('.popup-konfirmasi-pembelian');
const cardKonfirmasiPembelian = document.querySelector('.card-konfirmasi-pembelian');

function closePopupPurchaseConfirmation() {
	if (popupKonfirmasiPembelian.classList.contains('show') && cardKonfirmasiPembelian.classList.contains('show')) {
		popupKonfirmasiPembelian.classList.remove('show');
		cardKonfirmasiPembelian.classList.remove('show');
	} else {
		popupKonfirmasiPembelian.classList.add('show');
		cardKonfirmasiPembelian.classList.add('show');
	}
}

function eventClickCardPulsa() {
	const cardsPembelian = document.querySelectorAll('.card-pembelian');
	cardsPembelian.forEach((cardPembelian) => {
		cardPembelian.addEventListener('click', function (e) {
			e.preventDefault();

			const directLinkProduct = `${cardPembelian.dataset.link !== undefined ? `${cardPembelian.dataset.link}?catatan=${inputNomorTelepon.value}` : '#'}`;
			closePopupPurchaseConfirmation();

			let nominalPembelian;
			if (cardPembelian.classList.contains('card-pembelian--isi-pulsa')) {
				nominalPembelian = cardPembelian.querySelector('.pulsa').textContent;
			} else if (cardPembelian.classList.contains('card-pembelian--paket-data')) {
				nominalPembelian = cardPembelian.children[0].children[0].textContent;
			}

			const hargaProduct = cardPembelian.querySelector('.harga').textContent.split('Rp').join('').replace('.', '');
			const product = providerName.textContent;
			const biayaAdmin = 0;
			const totalPembayaran = parseInt(hargaProduct) + biayaAdmin;

			purchaseConfirmation(inputNomorTelepon.value, nominalPembelian, hargaProduct, product, biayaAdmin, totalPembayaran);

			btnPurchaseProduct(directLinkProduct);
		});
	});
}

function btnPurchaseProduct(link) {
	const tipeProduk = document.querySelector('.konfirmasi-tipe-produk').textContent;
	const nominal = document.querySelector('.konfirmasi-nominal').textContent.split(' ').join('-');

	const btnKonfirmasiPembelian = document.querySelectorAll('.btn-konfirmasi-pembelian')[1];
	btnKonfirmasiPembelian.addEventListener('click', function () {
		if (link === '#') {
			alert('Link belum ada tunggu nanti ya...');
		} else {
			btnKonfirmasiPembelian.href = link.split(' ').join('');
		}
	});
}

document.addEventListener('click', function (e) {
	if (e.target.classList.contains('popup-konfirmasi-pembelian') || e.target.classList.contains('btn-konfirmasi-pembelian--ubah')) {
		closePopupPurchaseConfirmation();
	}
});

function purchaseConfirmation(nomorTujuan, nominalPembelian, hargaProduct, product, biayaAdmin, totalPembayaran) {
	const konfirmasiNomorTujuan = document.querySelector('.konfirmasi-nomor-tujuan');
	const konfirmasiNominal = document.querySelector('.konfirmasi-nominal');
	const konfirmasiTipeProduk = document.querySelector('.konfirmasi-tipe-produk');
	const konfirmasiBiayaAdmin = document.querySelector('.konfirmasi-biaya-admin');
	const konfirmasiHargaProduct = document.querySelector('.konfirmasi-harga-product');

	konfirmasiNomorTujuan.textContent = nomorTujuan.split(' ').join('');

	const nominalHargaProduct = parseInt(hargaProduct).toLocaleString().replace(',', '.');

	konfirmasiNominal.textContent = nominalPembelian;
	konfirmasiHargaProduct.textContent = `Rp${nominalHargaProduct}`;
	konfirmasiTipeProduk.textContent = product;
	konfirmasiBiayaAdmin.textContent = biayaAdmin;

	const convertTotalPembayaran = totalPembayaran.toLocaleString().replace(',', '.');
	const konfirmasiTotalPembayaran = document.querySelector('.konfirmasi-total-pembayaran');
	konfirmasiTotalPembayaran.textContent = `Rp${convertTotalPembayaran}`;
}

const body = document.body;
const minimNoHPInput = document.querySelector('.minim-nohp');

const minimNoHPTrigger = () => {
	if (inputNomorTelepon.value.length >= 1 && inputNomorTelepon.value.length < 9) {
		minimNoHPInput.classList.add('show');
	} else {
		minimNoHPInput.classList.remove('show');
	}
};

const dataSuccess = () => {
	conditionUpdateUIProvider(inputNomorTelepon.value);
	body.classList.add('active');
};

document.forms[0].addEventListener('submit', function (e) {
	e.preventDefault();
	inputNomorTelepon.blur();

	if (inputNomorTelepon.value.length < 9) {
		wrapperIsiPulsa.innerHTML = '';
		changeProviderContent();
		body.removeAttribute('class');
	} else {
		dataSuccess();
	}
});

inputNomorTelepon.addEventListener('keyup', function () {
	minimNoHPTrigger();
	clearInputTrigger();

	if (inputNomorTelepon.value.length >= 1 && inputNomorTelepon.value.length < 9) {
		inputNomorTelepon.style.background = '#ff000033';
	} else {
		inputNomorTelepon.removeAttribute('style');
	}
});

const pakaiNomor = document.querySelector('.pakai-nomor');
pakaiNomor.addEventListener('click', function () {
	const getPhoneNumberNow = this.dataset.nohp;
	inputNomorTelepon.value = getPhoneNumberNow;
	clearInputTrigger();
	dataSuccess();
});

inputNomorTelepon.addEventListener('paste', function () {
	setTimeout(() => {
		clearInputTrigger();
		dataSuccess();
	}, 0);
});
