const Providers = {
	aksi: {
		promo: 'Promo',
		'pilih-nominal': 'Pilih Nominal',
		'paket-data': 'Paket Data',
	},

	indosat: {
		'nama-provider': 'Indosat',
		logo: 'https://i.postimg.cc/T1wQy9Cf/image.png',

		'promo-provider': [
			{
				'nama-promo': 'INDOSAT 5.000 Promo',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '5.845',
				tersedia: true,
				link: 'https://google.com',
			},
			{
				'nama-promo': 'INDOSAT 10.000 Promo',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '9.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'pilih-nominal': [
			{
				'nama-nominal': 'INDOSAT 20.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '58.845',
				tersedia: true,
				link: 'https://instagram.com',
			},
			{
				'nama-nominal': 'INDOSAT 40.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '48.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'INDOSAT 100.000',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '100.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'paket-data': [
			{
				'nama-paket': 'Paket Sahur Murah Meriah',
				'promo-tersedia': false,
				description: [],
				harga: '20.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari', 'Paket Unlimited', 'Paket andelan Dijamin gaspol'],
				harga: '50.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL TERUSS',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '60.845',
				tersedia: false,
				link: 'https://facebook.com',
			},
		],
	},

	telkomsel: {
		'nama-provider': 'Telkomsel',
		logo: 'https://i.postimg.cc/jSBksXVW/image.png',

		'promo-provider': [
			{
				'nama-promo': 'TSEL 5.000 Promo',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '5.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-promo': 'TSEL 10.000 Promo',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '9.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'pilih-nominal': [
			{
				'nama-nominal': 'TSEL 20.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '58.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'TSEL 40.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '48.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'TSEL 100.000',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '100.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'paket-data': [
			{
				'nama-paket': 'Paket Sahur Murah Meriah',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '20.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '50.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL TERUSS',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '60.845',
				tersedia: false,
				link: 'https://facebook.com',
			},
		],
	},

	xl: {
		'nama-provider': 'XL',
		logo: 'https://i.postimg.cc/wvN1C9Xy/image.png',

		'promo-provider': [
			{
				'nama-promo': 'XL 5.000 Promo',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '5.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-promo': 'XL 10.000 Promo',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '9.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'pilih-nominal': [
			{
				'nama-nominal': 'XL 20.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '58.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'XL 40.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '48.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'XL 100.000',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '100.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'paket-data': [
			{
				'nama-paket': 'Paket Sahur Murah Meriah',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '20.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '50.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL TERUSS',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '60.845',
				tersedia: false,
				link: 'https://facebook.com',
			},
		],
	},

	tri: {
		'nama-provider': 'Tri',
		logo: 'https://i.postimg.cc/j5S8ckPF/image.png',

		'promo-provider': [
			{
				'nama-promo': 'Tri 5.000 Promo',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '5.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-promo': 'Tri 10.000 Promo',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '9.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'pilih-nominal': [
			{
				'nama-nominal': 'Tri 20.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '58.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'Tri 40.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '48.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'Tri 100.000',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '100.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'paket-data': [
			{
				'nama-paket': 'Paket Sahur Murah Meriah',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '20.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '50.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL TERUSS',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '60.845',
				tersedia: false,
				link: 'https://facebook.com',
			},
		],
	},

	smartfren: {
		'nama-provider': 'Smartfren',
		logo: 'https://i.postimg.cc/bNhmFJxn/image.png',

		'promo-provider': [
			{
				'nama-promo': 'Smartfren 5.000 Promo',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '5.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-promo': 'Smartfren 10.000 Promo',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '9.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'pilih-nominal': [
			{
				'nama-nominal': 'Smartfren 20.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '58.845',
				tersedia: true,
				link: '',
			},
			{
				'nama-nominal': 'Smartfren 40.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '48.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'Smartfren 100.000',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '100.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'paket-data': [
			{
				'nama-paket': 'Paket Sahur Murah Meriah',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '20.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '50.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL TERUSS',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '60.845',
				tersedia: false,
				link: 'https://facebook.com',
			},
		],
	},

	axis: {
		'nama-provider': 'Axis',
		logo: 'https://i.postimg.cc/zXGJWJyC/image.png',

		'promo-provider': [
			{
				'nama-promo': 'Axis 5.000 Promo',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '5.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-promo': 'Axis 10.000 Promo',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '9.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'pilih-nominal': [
			{
				'nama-nominal': 'Axis 20.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '58.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'Axis 40.000',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '48.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-nominal': 'Axis 100.000',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '100.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
		],

		'paket-data': [
			{
				'nama-paket': 'Paket Sahur Murah Meriah',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '20.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL',
				'promo-tersedia': true,
				description: ['Masa Aktif 7 Hari'],
				harga: '50.845',
				tersedia: true,
				link: 'https://facebook.com',
			},
			{
				'nama-paket': 'Paket GAS POLL TERUSS',
				'promo-tersedia': false,
				description: ['Masa Aktif 7 Hari'],
				harga: '60.845',
				tersedia: false,
				link: 'https://facebook.com',
			},
		],
	},
};
