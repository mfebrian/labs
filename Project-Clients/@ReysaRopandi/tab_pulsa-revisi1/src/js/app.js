const wrapperSubProvider = document.querySelector('.wrapper-sub-provider');

function providerUI(action) {
	return `<div class="action" data-target="${action}"></div>`;
}

function allAction() {
	const actions = Providers.aksi;
	let temp = '';
	for (let action in actions) {
		temp += providerUI(actions[action]);
	}

	wrapperSubProvider.innerHTML = temp;
}
allAction();

function actionUIStructure(providerName, provider, action) {
	const descriptions = provider['description'];
	return `
				<div class="wrapper-card-action">
				<a href="${provider.link == undefined || provider.link == '' ? '#' : `${provider.link}`}" class="wrapper-sub-action">
						<div class="position-left">
							<img src="${providerName.logo}" alt="${providerName['nama-provider']}" class="logo-provider" />
							<div class="wrapper-action-provider">
								<span class="title-action-provider">${action}</span>
								<span class="promo">${provider['promo-tersedia'] === true ? 'Promo' : ''}</span>
								<span class="active-period">
								<ul class="list-active-period">
								${descriptions.length != 0 ? allDescription(descriptions) : ''}
								</ul>
								</span>
							</div>
						</div>
						<div class="position-right">
							<span class="price-action">${provider.harga}</span>
							<span class="${provider.tersedia === true ? 'available' : 'not-available'}">${provider.tersedia === true ? 'Tersedia!' : 'Belum Tersedia!'}</span>
						</div>
					</a>
				</div>`;
}

function allDescription(descriptions) {
	return descriptions.map((description) => `<li>- ${description}</li>`).join('');
}

// Bagian promo
const promo = document.querySelector('[data-target="Promo"');
function promoProvider(providerName, providers) {
	let tempStructure = '';
	providers.forEach((provider) => {
		tempStructure += actionUIStructure(providerName, provider, provider['nama-promo']);
	});
	promo.innerHTML = `
		<span class="title-action">${Providers.aksi['promo']}</span>
	${tempStructure}`;
}
// Akhir Bagian Promo

// Bagian Pilihan Nominal
const pilihNominal = document.querySelector('[data-target="Pilih Nominal"');
function pilihNominalProvider(providerName, providers) {
	let tempStructure = '';
	providers.forEach((provider) => {
		tempStructure += actionUIStructure(providerName, provider, provider['nama-nominal']);
	});
	pilihNominal.innerHTML = `
	<span class="title-action">${Providers.aksi['pilih-nominal']}</span>
	${tempStructure}`;
}
// Ahkhir bagian Pilihan Nominal

// Bagian Paket Data
const paketData = document.querySelector('[data-target="Paket Data"');
function paketDataProvider(providerName, providers) {
	let tempStructure = '';
	providers.forEach((provider) => {
		tempStructure += actionUIStructure(providerName, provider, provider['nama-paket']);
	});
	paketData.innerHTML = `
	<span class="title-action">${Providers.aksi['paket-data']}</span>
	${tempStructure}`;
}
// Ahkhir bagian Paket Data

function joinAllActions(providerName) {
	if (providerName !== undefined) {
		promoProvider(providerName, providerName['promo-provider']);
		pilihNominalProvider(providerName, providerName['pilih-nominal']);
		paketDataProvider(providerName, providerName['paket-data']);
	}
}

function conditionUpdateUIProvider(phoneNumber) {
	if (phoneNumber.length <= 3) {
		// wrapperSubProvider.innerHTML = `<div class="wrapper-provider-error">
		// 						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="72" height="72">
		// 						<path fill="none" d="M0 0h24v24H0z" />
		// 						<path
		// 							d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm0-11.414L9.172 7.757 7.757 9.172 10.586 12l-2.829 2.828 1.415 1.415L12 13.414l2.828 2.829 1.415-1.415L13.414 12l2.829-2.828-1.415-1.415L12 10.586z"
		// 							fill="rgba(255,11,11,1)"
		// 						/>
		// 					</svg>
		// 					<span>Ups... Provider tidak ditemukan!</span>
		// 				</div>`;
	} else if (phoneNumber.length >= 4) {
		const getFourDigitPhoneNumber = phoneNumber.slice(0, 4);

		// Nomor telepon Telkomsel
		if (
			getFourDigitPhoneNumber == 0811 ||
			getFourDigitPhoneNumber == 0812 ||
			getFourDigitPhoneNumber == 0813 ||
			getFourDigitPhoneNumber == 0821 ||
			getFourDigitPhoneNumber == 0822 ||
			getFourDigitPhoneNumber == 0852 ||
			getFourDigitPhoneNumber == 0853 ||
			getFourDigitPhoneNumber == 0823 ||
			getFourDigitPhoneNumber == 0851 ||
			getFourDigitPhoneNumber == 0835
		) {
			const telkomsel = Providers.telkomsel;
			joinAllActions(telkomsel);

			// Nomor Axis
		} else if (getFourDigitPhoneNumber == 0838 || getFourDigitPhoneNumber == 0831 || getFourDigitPhoneNumber == 0832 || getFourDigitPhoneNumber == 0833) {
			const axis = Providers.axis;
			joinAllActions(axis);
			// Nomor Tri
		} else if (getFourDigitPhoneNumber == 0895 || getFourDigitPhoneNumber == 0896 || getFourDigitPhoneNumber == 0897 || getFourDigitPhoneNumber == 0898 || getFourDigitPhoneNumber == 0899) {
			const tri = Providers.tri;
			joinAllActions(tri);

			// Indosat
		} else if (
			getFourDigitPhoneNumber == 0814 ||
			getFourDigitPhoneNumber == 0815 ||
			getFourDigitPhoneNumber == 0816 ||
			getFourDigitPhoneNumber == 0855 ||
			getFourDigitPhoneNumber == 0856 ||
			getFourDigitPhoneNumber == 0857 ||
			getFourDigitPhoneNumber == 0858
		) {
			const indosat = Providers.indosat;
			joinAllActions(indosat);

			// Nomor XL
		} else if (getFourDigitPhoneNumber == 0818 || getFourDigitPhoneNumber == 0817 || getFourDigitPhoneNumber == 0819 || getFourDigitPhoneNumber == 0859 || getFourDigitPhoneNumber == 0877 || getFourDigitPhoneNumber == 0878) {
			const xl = Providers.xl;
			joinAllActions(xl);

			// Nomor Smartfreen
		} else if (
			getFourDigitPhoneNumber == 0881 ||
			getFourDigitPhoneNumber == 0882 ||
			getFourDigitPhoneNumber == 0883 ||
			getFourDigitPhoneNumber == 0884 ||
			getFourDigitPhoneNumber == 0885 ||
			getFourDigitPhoneNumber == 0886 ||
			getFourDigitPhoneNumber == 0887 ||
			getFourDigitPhoneNumber == 0888 ||
			getFourDigitPhoneNumber == 0889
		) {
			const smartfren = Providers.smartfren;
			joinAllActions(smartfren);
		}
	}
}

function doneInputNumber() {
	let phoneNumber = inputNomorTelepon.value;
	conditionUpdateUIProvider(phoneNumber);
}

const inputNomorTelepon = document.querySelector('.input-nomor-telepon');
inputNomorTelepon.addEventListener('keyup', function () {
	doneInputNumber();
});

inputNomorTelepon.addEventListener('paste', function () {
	doneInputNumber();
});

const popupProvider = document.querySelector('.popup-provider');

const formNomorTelepon = document.querySelector('.form-nomor-telepon');
formNomorTelepon.addEventListener('submit', function (e) {
	e.preventDefault();
	if (inputNomorTelepon.value.length >= 4) {
		popupProvider.classList.add('show');
	}

	inputNomorTelepon.blur();
	eventClickCardPulsa();
});

const wrapperProvider = document.querySelector('.wrapper-provider');
const screenProvider = document.querySelector('.screen-provider');
screenProvider.addEventListener('click', function () {
	let dataFullscreen = screenProvider.dataset.fullscreen;

	if (dataFullscreen == 'false') {
		screenProvider.dataset.fullscreen = 'true';
		screenProvider.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M18 7h4v2h-6V3h2v4zM8 9H2V7h4V3h2v6zm10 8v4h-2v-6h6v2h-4zM8 15v6H6v-4H2v-2h6z" fill="rgba(38,38,38,1)"/></svg>`;
	} else {
		screenProvider.dataset.fullscreen = 'false';
		screenProvider.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20">
							<path fill="none" d="M0 0h24v24H0z" />
							<path d="M20 3h2v6h-2V5h-4V3h4zM4 3h4v2H4v4H2V3h2zm16 16v-4h2v6h-6v-2h4zM4 19h4v2H2v-6h2v4z" fill="rgba(38,38,38,1)" />
						</svg>`;
	}

	wrapperProvider.classList.toggle('full');
});

popupProvider.addEventListener('click', function (e) {
	if (e.target.classList.contains('popup-provider')) {
		this.classList.remove('show');
	}
});

const closePopupProvider = document.getElementById('close-popup-screen-provider');
closePopupProvider.addEventListener('click', function () {
	popupProvider.classList.remove('show');
});

const popupKonfirmasiPembelian = document.querySelector('.popup-konfirmasi-pembelian');
const cardKonfirmasiPembelian = document.querySelector('.card-konfirmasi-pembelian');

function closePopupPurchaseConfirmation() {
	if (popupKonfirmasiPembelian.classList.contains('show') && cardKonfirmasiPembelian.classList.contains('show')) {
		popupKonfirmasiPembelian.classList.remove('show');
		cardKonfirmasiPembelian.classList.remove('show');
	} else {
		popupKonfirmasiPembelian.classList.add('show');
		cardKonfirmasiPembelian.classList.add('show');
	}
}

function eventClickCardPulsa() {
	const cardsPembelian = document.querySelectorAll('.wrapper-sub-action');
	cardsPembelian.forEach((cardPembelian) => {
		cardPembelian.addEventListener('click', function (e) {
			e.preventDefault();

			const linkProvider = cardPembelian.href;
			const directLinkProduct = `${linkProvider}?catatan=${inputNomorTelepon.value}`;
			closePopupPurchaseConfirmation();

			const hargaProduct = cardPembelian.querySelector('.price-action').textContent.split('Rp').join('').replace('.', '');
			const product = cardPembelian.querySelector('img').alt;
			const biayaAdmin = 0;
			const totalPembayaran = parseInt(hargaProduct) + biayaAdmin;

			purchaseConfirmation(inputNomorTelepon.value, hargaProduct, product, biayaAdmin, totalPembayaran);

			btnPurchaseProduct(directLinkProduct);
		});
	});
}

function btnPurchaseProduct(link) {
	const btnKonfirmasiPembelian = document.querySelectorAll('.btn-konfirmasi-pembelian')[1];
	btnKonfirmasiPembelian.addEventListener('click', function () {
		btnKonfirmasiPembelian.href = link;
	});
}

document.addEventListener('click', function (e) {
	if (e.target.classList.contains('popup-konfirmasi-pembelian') || e.target.classList.contains('btn-konfirmasi-pembelian--ubah')) {
		closePopupPurchaseConfirmation();
	}
});

function purchaseConfirmation(nomorTujuan, hargaProduct, product, biayaAdmin, totalPembayaran) {
	const konfirmasiNomorTujuan = document.querySelector('.konfirmasi-nomor-tujuan');
	const konfirmasiNominal = document.querySelector('.konfirmasi-nominal');
	const konfirmasiTipeProduk = document.querySelector('.konfirmasi-tipe-produk');
	const konfirmasiBiayaAdmin = document.querySelector('.konfirmasi-biaya-admin');
	const konfirmasiHargaProduct = document.querySelector('.konfirmasi-harga-product');

	konfirmasiNomorTujuan.textContent = nomorTujuan.split(' ').join('');

	const nominalHargaProduct = parseInt(hargaProduct).toLocaleString().replace(',', '.');

	konfirmasiNominal.textContent = nominalHargaProduct;
	konfirmasiHargaProduct.textContent = `Rp${nominalHargaProduct}`;
	konfirmasiTipeProduk.textContent = product;
	konfirmasiBiayaAdmin.textContent = biayaAdmin;

	const convertTotalPembayaran = totalPembayaran.toLocaleString().replace(',', '.');
	const konfirmasiTotalPembayaran = document.querySelector('.konfirmasi-total-pembayaran');
	konfirmasiTotalPembayaran.textContent = `Rp${convertTotalPembayaran}`;
}
