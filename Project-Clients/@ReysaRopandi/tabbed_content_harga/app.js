// Ambil element input
const inputPrice = document.querySelector('#harga-pengisian');

// Ambil semua pilihan harga
const prices = document.querySelectorAll('.pembungkus-harga-pengisian span');
prices.forEach((price) => {
	price.addEventListener('click', function () {
		// Ambil harga nya ketika di klik
		const getTextPrice = price.textContent.replaceAll('.', '');
		inputPrice.value = getTextPrice;
	});
});
