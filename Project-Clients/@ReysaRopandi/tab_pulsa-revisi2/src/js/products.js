const Products = {
	'card-product': [
		{
			'logo-product': 'https://lh3.googleusercontent.com/nvYlsHWiDicue6PzGzZU7f-F4XLRWQejNqaYrwX1-Hrrsq8XuFqKdW2q31hJJXptEg',
			'name-product': 'Digital Spin',
			'description-product': 'Produk Digital Spin',
		},
		{
			'logo-product': 'https://play-lh.googleusercontent.com/qDgRvPvic0GQ5hqxxlmOiUD5ZLiqB_7yp4rvwA34IhZEZVNrZG9UmR4qcqv2D2x4EX6Y',
			'name-product': 'i-Saku Indomaret',
			'description-product': 'Produk i-Saku Indomaret',
		},
		{
			'logo-product': 'https://kaspro.id/style/images/Logo%20KasPro%20Purple.png',
			'name-product': 'Kaspro',
			'description-product': 'Produk Kaspro',
		},
		{
			'logo-product': 'https://static-hermes.bukalapak.com/images/logos/mitra-bl-square.svg',
			'name-product': 'Mitra Bukalapak',
			'description-product': 'Produk Mitra Bukalapak',
		},
		{
			'logo-product': 'https://mitra.shopee.co.id/share.jpg',
			'name-product': 'Mitra Shopee',
			'description-product': 'Produk Mitra Shopee',
		},
		{
			'logo-product': 'https://1.bp.blogspot.com/-ma4pJeD9mBs/YIGBRB4lb0I/AAAAAAAAG5o/uZyGnNeC3SwUd6sma-1ZobPfCX9OOyxogCLcBGAsYHQ/w0/Mitra-Tokopedia.webp',
			'name-product': 'Mitra Tokopedia',
			'description-product': 'Produk Mitra Tokopedia',
		},
		{
			'logo-product': 'https://play-lh.googleusercontent.com/QbTMWaLUMr6xjwBLONHF6iff3v3hYTXZMqBdIvkHlRHyFzS9uluN4Rnhsn5DHQiZLg',
			'name-product': 'Sakuku BCA',
			'description-product': 'Produk Sakuku BCA',
		},
		{
			'logo-product': 'https://asset.kompas.com/crops/j9nQwcliZprhcMAnNelfQ1dhN3U=/0x0:780x390/750x500/data/photo/2013/07/05/2202006doku780x390.jpg',
			'name-product': 'Saldo DOKU',
			'description-product': 'Pastikan nomor yang ditransaksikan sudah terdaftar di aplikasi DOKU',
		},
		{
			'logo-product': 'https://play-lh.googleusercontent.com/v0UW49SrkxIzfRRhYArIJvP456-QeKT9-1Yxk19gwJESPidGAnJS7n7_sHZe81NpX_E',
			'name-product': 'Saldo DANA',
			'description-product': 'Produk Saldo DANA',
		},
		{
			'logo-product': 'https://gopay.co.id/icon.png',
			'name-product': 'Saldo Gopay',
			'description-product': 'Produk Saldo Gopay',
		},
		{
			'logo-product': 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/85/LinkAja.svg/1200px-LinkAja.svg.png',
			'name-product': 'Saldo LinkAja',
			'description-product': 'Produk Saldo LinkAja',
		},
		{
			'logo-product': 'https://images.bisnis-cdn.com/posts/2019/11/14/1170252/images-51-600x400.jpeg',
			'name-product': 'Saldo M-Tix CINEMA XXI',
			'description-product': 'Produk Saldo M-Tix CINEMA XXI',
		},
		{
			'logo-product': 'https://theme.zdassets.com/theme_assets/1379487/2cb35fe96fa1191f49c2b769b50cf8b546fff65e.png',
			'name-product': 'Saldo OVO',
			'description-product': 'Memasukkan nomor HP yang terdaftar di OVO',
		},
		{
			'logo-product': 'https://play-lh.googleusercontent.com/ZSoki_bt6_-p97BlfBVYBHe5Uuv1DJOfXfvG8PVM-3EfsbmidMhZ8yAn6NoBh6bcjJw',
			'name-product': 'Shopee Driver',
			'description-product': 'Produk Shopee Driver',
		},
		{
			'logo-product': 'https://asset-a.grid.id/crop/0x79:499x436/700x465/photo/2021/09/19/whatsapp-image-2021-09-19-at-17-20210919110149.jpeg',
			'name-product': 'Shopee Pay',
			'description-product': 'Produk Shopee Pay',
		},
		{
			'logo-product': 'https://res.cloudinary.com/crunchbase-production/image/upload/c_lpad,f_auto,q_auto:eco,dpr_1/v1463602429/hseqthtzwhrd9hatu9be.png',
			'name-product': 'Grab',
			'description-product': 'Produk Grab',
		},

		// Tutup Card Products
	],

	'data-products': {
		'digital-spin': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Digital Spin 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Digital Spin 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Digital Spin 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Digital Spin 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Digital Spin 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Digital Spin 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Digital Spin 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'i-saku-indomaret': {
			info: null,
			'choose-products': [
				{
					'name-product': 'i-Saku Indomaret 20.000',
					harga: '20.000',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'i-Saku Indomaret 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'i-Saku Indomaret 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'i-Saku Indomaret 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'i-Saku Indomaret 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'i-Saku Indomaret 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'i-Saku Indomaret 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		kaspro: {
			info: null,
			'choose-products': [
				{
					'name-product': 'Kaspro 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Kaspro 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Kaspro 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Kaspro 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Kaspro 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Kaspro 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Kaspro 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'mitra-bukalapak': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Mitra Bukalapak 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Bukalapak 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Bukalapak 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Bukalapak 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Bukalapak 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Bukalapak 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Bukalapak 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'mitra-shopee': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Mitra Shopee 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Shopee 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Shopee 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Shopee 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Shopee 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Shopee 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Shopee 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'mitra-tokopedia': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Mitra Tokopedia 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Tokopedia 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Tokopedia 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Tokopedia 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Tokopedia 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Tokopedia 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Mitra Tokopedia 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'sakuku-bca': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Sakuku BCA 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Sakuku BCA 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Sakuku BCA 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Sakuku BCA 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Sakuku BCA 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Sakuku BCA 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Sakuku BCA 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'saldo-doku': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Saldo DOKU 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DOKU 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DOKU 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DOKU 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DOKU 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DOKU 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DOKU 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'saldo-dana': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Saldo DANA 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DANA 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DANA 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DANA 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DANA 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DANA 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo DANA 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'saldo-gopay': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Saldo Gopay 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo Gopay 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo Gopay 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo Gopay 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo Gopay 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo Gopay 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo Gopay 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'saldo-linkaja': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Saldo LinkAja 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo LinkAja 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo LinkAja 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo LinkAja 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo LinkAja 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo LinkAja 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo LinkAja 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'saldo m-tix-cinema-xxi': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo M-Tx CINEMA XXI 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'saldo-ovo': {
			info: 'Masukkan nomor HP yang terdaftar di OVO. Saldo yang masuk akan dikurangi -1.000 sebagai biaya admin ke nomor tujuan. Contoh produk OVO 20.000 masuk 19.000',
			'choose-products': [
				{
					'name-product': 'Saldo OVO 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo OVO 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo OVO 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo OVO 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo OVO 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo OVO 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Saldo OVO 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'shopee-driver': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Shopee Driver 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Driver 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Driver 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Driver 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Driver 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Driver 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Driver 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		'shopee-pay': {
			info: null,
			'choose-products': [
				{
					'name-product': 'Shopee Pay 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Pay 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Pay 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Pay 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Pay 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Pay 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Shopee Pay 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		grab: {
			info: 'Ini adalah info Grab',
			'choose-products': [
				{
					'name-product': 'Grab 20.000',
					harga: '19.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Grab 30.000',
					harga: '29.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Grab 40.000',
					harga: '39.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Grab 50.000',
					harga: '49.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Grab 60.000',
					harga: '59.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Grab 70.000',
					harga: '69.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
				{
					'name-product': 'Grab 80.000',
					harga: '79.955',
					tersedia: true,
					link: 'https://google.com/blabla',
				},
			],
		},

		// Tutup Data Products
	},
};
