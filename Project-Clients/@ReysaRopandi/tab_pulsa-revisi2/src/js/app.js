const wrapperProducts = document.querySelector('.wrapper-products');
function printCardProducts() {
	let temp = '';
	Products['card-product'].forEach((product) => {
		const attributeDataProduct = product['name-product'].toLowerCase().split(' ').join('-');

		temp += `<div class="card-product card-product--popup" data-product="${attributeDataProduct}">
						<div class="section-product">
							<img src="${product['logo-product']}" alt="${attributeDataProduct}" class="img-product" />
							<div>
								<p class="name-product">${product['name-product']}</p>
								<span class="description-product">${product['description-product']}</span>
							</div>
						</div>
					</div>`;
	});

	wrapperProducts.innerHTML = temp;
}

printCardProducts();

const phoneNumber = document.getElementById('nomor-telepon');
// Hapus nomor telepon di input
const clearPhoneNumber = document.getElementById('clear-input');
clearPhoneNumber.addEventListener('click', function () {
	phoneNumber.value = '';

	clearPhoneNumber.removeAttribute('style');
});

// Input nomor telepon ketika diisi
phoneNumber.addEventListener('keyup', function () {
	if (phoneNumber.value != '') {
		clearPhoneNumber.style.display = 'block';
	} else {
		clearPhoneNumber.removeAttribute('style');
	}
});

// Tampilin popup semua product
const cardChooseProduct = document.querySelector('.card-choose-product');
const popupAllProduct = document.querySelector('.popup-all-product');
cardChooseProduct.addEventListener('click', function () {
	popupAllProduct.classList.add('show');
	document.body.style.overflow = 'hidden';
});

// Bagian cari product
const searchProduct = document.querySelector('.search-product');
searchProduct.addEventListener('keyup', function () {
	const lowerCaseValue = this.value.toLowerCase();

	const productsName = document.querySelectorAll('.popup-all-product .name-product');
	productsName.forEach((productName) => {
		const title = productName.textContent.toLowerCase();

		const parentProductName = productName.parentElement.parentElement.parentElement;
		if (title.indexOf(lowerCaseValue) != -1) {
			parentProductName.style.display = 'block';
		} else {
			parentProductName.style.display = 'none';
		}
	});
});

// Hilangin popup semua product
const closePopupProduct = document.querySelector('.close-popup-product');
closePopupProduct.addEventListener('click', function () {
	popupAllProduct.classList.remove('show');
	document.body.removeAttribute('style');
});

// Nampilin product yang dipilih
const wrapperChooseProduct = document.querySelector('.wrapper-choose-product');
function productSelected(products, logoProduct) {
	let chooseProduct = '';
	products['choose-products'].forEach((product) => (chooseProduct += productsSelectedLoop(product, logoProduct)));

	wrapperChooseProduct.innerHTML = `${
		products['info'] !== null
			? `<div class="wrapper-info-product">
					<p>
						<span class="text-info">Info:</span>
						${products['info']}
					</p>
				</div>`
			: ''
	}

	${chooseProduct}`;
}

function productsSelectedLoop(products, logoProduct) {
	return `<a href="${products['link']}" class="card-product">
					<div class="section-product">
						<img src="${logoProduct.src}" alt="${logoProduct.alt}" class="img-product" />
						<p class="name-product">${products['name-product']}</p>
					</div>

					<div class="section-product section-right">
						<span class="text-choose-product price">Rp${products['harga']}</span>
						${products['tersedia'] === true ? `<span class="available">Tersedia!</span>` : `<span class="not-available">Tidak Tersedia!</span>`}
					</div>
				</a>`;
}

const cardsProduct = wrapperProducts.querySelectorAll('.card-product');
cardsProduct.forEach((cardProduct) => {
	cardProduct.addEventListener('click', function () {
		const dataset = this.dataset.product;
		const product = Products['data-products'][dataset];

		popupAllProduct.classList.remove('show');
		document.body.removeAttribute('style');

		const nameProduct = this.querySelector('.name-product').textContent;
		const logoProduct = this.querySelector('.img-product');
		cardChooseProduct.innerHTML = changeCardChooseProduct(nameProduct, logoProduct);

		productSelected(product, logoProduct);
		popupAllProduct.scrollTo(0, 0);

		eventClickCardPulsa();
	});
});

function changeCardChooseProduct(nameProduct, logoProduct) {
	return `<div class="card-product">
						<div class="section-product">
							<img src="${logoProduct.src}" alt="${logoProduct.alt}" class="img-product" />
							<p class="name-product">${nameProduct}</p>
						</div>

						<div class="section-product section-right">
							<span class="text-choose-product">Pilih Product</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20">
								<path fill="none" d="M0 0h24v24H0z" />
								<path d="M13.172 12l-4.95-4.95 1.414-1.414L16 12l-6.364 6.364-1.414-1.414z" fill="rgba(38,38,38,1)" />
							</svg>
						</div>
					</div>`;
}

// Popup konfirmasi pembelian
const popupKonfirmasiPembelian = document.querySelector('.popup-konfirmasi-pembelian');
const cardKonfirmasiPembelian = document.querySelector('.card-konfirmasi-pembelian');

function closePopupPurchaseConfirmation() {
	if (popupKonfirmasiPembelian.classList.contains('show') && cardKonfirmasiPembelian.classList.contains('show')) {
		popupKonfirmasiPembelian.classList.remove('show');
		cardKonfirmasiPembelian.classList.remove('show');
	} else {
		popupKonfirmasiPembelian.classList.add('show');
		cardKonfirmasiPembelian.classList.add('show');
	}
}

function eventClickCardPulsa() {
	const cardsPembelian = wrapperChooseProduct.querySelectorAll('.card-product');
	cardsPembelian.forEach((cardPembelian) => {
		cardPembelian.addEventListener('click', function (e) {
			e.preventDefault();

			const linkProvider = cardPembelian.href;
			const directLinkProduct = `${linkProvider}?catatan=${phoneNumber.value}`;
			closePopupPurchaseConfirmation();

			const hargaProduct = cardPembelian.querySelector('.price').textContent.split('Rp').join('').replace('.', '');
			const product = cardPembelian.querySelector('.name-product').textContent;
			const biayaAdmin = 0;
			const totalPembayaran = parseInt(hargaProduct) + biayaAdmin;

			purchaseConfirmation(phoneNumber.value, hargaProduct, product, biayaAdmin, totalPembayaran);

			btnPurchaseProduct(directLinkProduct);
		});
	});
}

function btnPurchaseProduct(link) {
	const btnKonfirmasiPembelian = document.querySelectorAll('.btn-konfirmasi-pembelian')[1];
	btnKonfirmasiPembelian.addEventListener('click', function () {
		btnKonfirmasiPembelian.href = link;
	});
}

document.addEventListener('click', function (e) {
	if (e.target.classList.contains('popup-konfirmasi-pembelian') || e.target.classList.contains('btn-konfirmasi-pembelian--ubah')) {
		closePopupPurchaseConfirmation();
	}
});

function purchaseConfirmation(nomorTujuan, hargaProduct, product, biayaAdmin, totalPembayaran) {
	const konfirmasiNomorTujuan = document.querySelector('.konfirmasi-nomor-tujuan');
	const konfirmasiNominal = document.querySelector('.konfirmasi-nominal');
	const konfirmasiTipeProduk = document.querySelector('.konfirmasi-tipe-produk');
	const konfirmasiBiayaAdmin = document.querySelector('.konfirmasi-biaya-admin');
	const konfirmasiHargaProduct = document.querySelector('.konfirmasi-harga-product');

	konfirmasiNomorTujuan.textContent = nomorTujuan.split(' ').join('');

	const nominalHargaProduct = parseInt(hargaProduct).toLocaleString().replace(',', '.');

	konfirmasiNominal.textContent = nominalHargaProduct;
	konfirmasiHargaProduct.textContent = `Rp${nominalHargaProduct}`;
	konfirmasiTipeProduk.textContent = product;
	konfirmasiBiayaAdmin.textContent = biayaAdmin;

	const convertTotalPembayaran = totalPembayaran.toLocaleString().replace(',', '.');
	const konfirmasiTotalPembayaran = document.querySelector('.konfirmasi-total-pembayaran');
	konfirmasiTotalPembayaran.textContent = `Rp${convertTotalPembayaran}`;
}
