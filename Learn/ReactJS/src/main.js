const root = ReactDOM.createRoot(document.getElementById('root'));

// React.useState
// function App() {
// 	const [count, setUpdate] = React.useState(0);
// 	return (
// 		<>
// 			<button
// 				onClick={function () {
// 					if (count > 0) {
// 						setUpdate(count - 1);
// 					}
// 				}}
// 			>
// 				-
// 			</button>
// 			<span>{count}</span>
// 			<button
// 				onClick={function () {
// 					setUpdate(count + 1);
// 				}}
// 			>
// 				+
// 			</button>
// 		</>
// 	);
// }

function App() {
	const [klik, setTrue] = React.useState(false);
	const [count, setCount] = React.useState(0);

	React.useEffect(
		function () {
			console.log(document.getElementById('judul'));
		},
		[klik, count]
	);
	return (
		<>
			<h1 id="judul">Hello World</h1>
			<button
				onClick={function () {
					setTrue(true);
				}}
			>
				Set true
			</button>
			<button
				onClick={function () {
					setCount(count + 1);
				}}
			>
				Tambah
			</button>
			Nilai saat ini: {count}
		</>
	);
}

root.render(<App />);
