const root = ReactDOM.createRoot(document.getElementById('root')); // React.useState
// function App() {
// 	const [count, setUpdate] = React.useState(0);
// 	return (
// 		<>
// 			<button
// 				onClick={function () {
// 					if (count > 0) {
// 						setUpdate(count - 1);
// 					}
// 				}}
// 			>
// 				-
// 			</button>
// 			<span>{count}</span>
// 			<button
// 				onClick={function () {
// 					setUpdate(count + 1);
// 				}}
// 			>
// 				+
// 			</button>
// 		</>
// 	);
// }

function App() {
  const [klik, setTrue] = React.useState(false);
  const [count, setCount] = React.useState(0);
  React.useEffect(function () {
    console.log(document.getElementById('judul'));
  }, [klik, count]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("h1", {
    id: "judul"
  }, "Hello World"), /*#__PURE__*/React.createElement("button", {
    onClick: function () {
      setTrue(true);
    }
  }, "Set true"), /*#__PURE__*/React.createElement("button", {
    onClick: function () {
      setCount(count + 1);
    }
  }, "Tambah"), "Nilai saat ini: ", count);
}

root.render( /*#__PURE__*/React.createElement(App, null));