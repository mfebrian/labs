// // 1. With Looping
// // function fizzBuzz(value) {
// //    for (let i = 1; i <= value; i++) {
// //       if (i % 3 === 0 && i % 5 === 0) {
// //          console.log('Fizz Buzz');
// //       } else if (i % 3 === 0) {
// //          console.log('Fizz');
// //       } else if (i % 5 === 0) {
// //          console.log('Buzz');
// //       } else {
// //          console.log(i);
// //       }
// //    }
// // }

// // fizzBuzz(30);

// // 2. With Recursion
// function fizzBuzz(value) {
//    if (value <= 0) return;

//    if (value % 5 === 0 && value % 3 === 0) {
//       console.log('Fizz Buzz');
//    } else if (value % 5 === 0) {
//       console.log('Buzz');
//    } else if (value % 3 === 0) {
//       console.log('Fizz');
//    } else {
//       console.log(value);
//    }

//    fizzBuzz(value - 1);
// }

// fizzBuzz(30);
