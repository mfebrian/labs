// function palindrome(value) {
//    let temp = '';
//    for (let i = value.length - 1; i >= 0; i--) {
//       temp += value[i];
//    }

//    if (temp.toLowerCase() === value.toLowerCase()) {
//       return true;
//    } else {
//       return false;
//    }
// }

// console.log(palindrome('Katak'));
// function palindrome(value) {
//    for (let i = 0; i <= value.length / 2 - 1; i++) {
//       return value[i] === value[value.length - i - 1];
//    }
// }

// console.log(palindrome('safa'));
// console.log(palindrome('kodok'));

function palindrome(value, index) {
   if (index <= value.length / 2 - 1) {
      if (value[index] !== value[value.length - index - 1]) {
         return false;
      } else {
         return palindrome(value, index + 1);
      }
   } else {
      return true;
   }
}

console.log(palindrome('cicak', 0)); // false
console.log(palindrome('kodok', 0)); // true
