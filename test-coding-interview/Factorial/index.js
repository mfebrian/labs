// function factorial(value) {
//    let result = 1;
//    for (let i = value; i >= 1; i--) {
//       result *= i;
//    }

//    return result;
// }

// console.log(factorial(4));

function factorialRecursive(value) {
   return value === 1 ? value : value * factorialRecursive(value - 1);
}
console.log(factorialRecursive(100));

function factorialTailRecursive(value, total) {
   return value === 1 ? total : factorialTailRecursive(value - 1, total * value);
}
console.log(factorialTailRecursive(100, 1));
