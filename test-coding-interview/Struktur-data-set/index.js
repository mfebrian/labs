function setdata(value) {
   const notDuplicates = [];
      for (let i = 0; i < value.length; i++) {
         if (notDuplicates.indexOf(value[i]) === -1) {
            notDuplicates.push(value[i]);
         }
      }

      return notDuplicates.sort((a, b) => {
         return a - b;
      });
   return notDuplicates;
}

console.log(setdata([1, 2, 3, 4, 7, 5, 2, 2, 5, 6, 9, 10]));
