// Cara 1. Menggunakan variable temporary
function palindrome(value) {
   let temp = '';
   if (typeof value !== 'string') {
      return 'Masukkan string!';
   } else {
      for (let i = value.length - 1; i >= 0; i--) {
         temp += value[i];
      }
      return temp.toLowerCase() == value.toLowerCase();
   }
}

console.log(palindrome('kadal'));

// Cara 2. Tidak menggunakan variable temporary
function palindromeSecondWay(value) {
   if (typeof value !== 'string') {
      console.log('Masukkan string!');
   } else {
      for (let i = 0; i <= value.length - 1; i++) {
         return value[i] === value[value.length - 1 - i];
      }
   }
}

console.log(palindromeSecondWay('katak'));

// Cara 3. Tidak menggunakan variable temporary dan Optimize
function palindromeOptimize(value) {
   if (typeof value !== 'string') {
      console.log('Masukkan string!');
   } else {
      for (let i = 0; i <= value.length / 2 - 1; i++) {
         return value[i] === value[value.length - 1 - i];
      }
   }
}

console.log(palindromeOptimize('kutuutus'));

// Cara 4. Menggunakan Recursive
function palindromeRecursive(value, index) {
   if (typeof value !== 'string' || typeof index !== 'number') {
      return 'Masukkan string dan number!';
   } else {
      if (index < value.length / 2) {
         if (value[index] !== value[value.length - index - 1]) {
            return false;
         } else {
            return palindromeRecursive(value, index + 1);
         }
      } else {
         return true;
      }
   }
}

console.log(palindromeRecursive('katak', 0));
